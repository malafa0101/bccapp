package com.app.bccmobile.presentation.state

sealed class LoadingState {
    object Show : LoadingState()
    object Hide : LoadingState()
}