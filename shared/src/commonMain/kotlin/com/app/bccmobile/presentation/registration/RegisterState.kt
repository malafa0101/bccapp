package com.app.bccmobile.presentation.registration

sealed class RegisterState {
    data class Login(val phone: String) : RegisterState()
    data class Register(val phone: String) : RegisterState()
}