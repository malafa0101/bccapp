package com.app.bccmobile.presentation.login

sealed class LoginState {
    object Success : LoginState()
}