package com.app.bccmobile.presentation.profile

import com.app.bccmobile.domain.model.User

sealed class ProfileState {
    data class Success(val user: User) : ProfileState()
}