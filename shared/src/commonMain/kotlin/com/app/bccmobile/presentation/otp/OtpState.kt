package com.app.bccmobile.presentation.otp

sealed class OtpState {
    object Success : OtpState()
}
