package com.app.bccmobile.presentation.registration

import com.app.bccapp.domain.model.Response
import com.app.bccmobile.domain.use_case.CheckUserUseCase
import com.app.bccmobile.domain.use_case.SendOtpUseCase
import com.app.bccmobile.presentation.BaseViewModel
import com.app.bccmobile.presentation.state.LoadingState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class RegisterViewModel constructor(
    private val checkUserUseCase: CheckUserUseCase,
    private val sendOtpUseCase: SendOtpUseCase
) : BaseViewModel() {

    private val _registrationState = MutableStateFlow<RegisterState?>(null)
    val registrationState: StateFlow<RegisterState?> = _registrationState

    fun checkUser(phone: String) {
        viewModelScope.launch {
            _loadingState.emit(LoadingState.Show)
            val result = checkUserUseCase.checkUser(phone = phone)
            when(result) {
                is Response.Success -> {
                    if (result.data) {
                        _registrationState.value = RegisterState.Login(phone)
                    } else {
                        _registrationState.value = RegisterState.Register(phone)
                    }
                }
                is Response.Error -> {
                    _errorState.emit(result.error)
                }
            }
            _registrationState.value = null
            _loadingState.emit(LoadingState.Hide)
        }
    }
}