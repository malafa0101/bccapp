package com.app.bccmobile.presentation.registration_finish

import com.app.bccapp.domain.model.Response
import com.app.bccmobile.domain.use_case.RegistrationUseCase
import com.app.bccmobile.domain.use_case.ResetPasswordUseCase
import com.app.bccmobile.presentation.BaseViewModel
import com.app.bccmobile.presentation.state.LoadingState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class RegistrationFinishViewModel constructor(
    private val registrationUseCase: RegistrationUseCase,
    private val resetPasswordUseCase: ResetPasswordUseCase
) : BaseViewModel() {

    private val _registrationState = MutableStateFlow<RegisterFinishState?>(null)
    val registrationState: StateFlow<RegisterFinishState?> = _registrationState

    fun registration(otp: String, phone: String, password: String) = viewModelScope.launch {
        _loadingState.emit(LoadingState.Show)
        val result = registrationUseCase.register(
            param = RegistrationUseCase.Param(
                otp = otp,
                phone = phone,
                password = password
            )
        )
        when(result) {
            is Response.Success -> {
                _registrationState.emit(RegisterFinishState.Success)
            }
            is Response.Error -> {
                _errorState.emit(result.error)
            }
        }
        _registrationState.value = null
        _loadingState.emit(LoadingState.Hide)
    }

    fun resetPassword(otp: String, phone: String, password: String) = viewModelScope.launch {
        _loadingState.emit(LoadingState.Show)
        val result = resetPasswordUseCase.resetPassword(
            param = ResetPasswordUseCase.Param(
                otp = otp,
                phone = phone,
                password = password
            )
        )
        when(result) {
            is Response.Success -> {
                _registrationState.value = RegisterFinishState.ResetPassword(result.data)
            }
            is Response.Error -> {
                _errorState.emit(result.error)
            }
        }
        _registrationState.value = null
        _loadingState.emit(LoadingState.Hide)
    }
}