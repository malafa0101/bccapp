package com.app.bccmobile.presentation.pincode

import com.app.bccmobile.domain.use_case.pincode.GetPinCodeUseCase
import com.app.bccmobile.domain.use_case.pincode.IsPinCodeValidUseCase
import com.app.bccmobile.domain.use_case.pincode.SavePinCodeUseCase
import com.app.bccmobile.presentation.BaseViewModel
import dev.icerock.moko.biometry.BiometryAuthenticator
import dev.icerock.moko.mvvm.dispatcher.EventsDispatcher
import dev.icerock.moko.mvvm.dispatcher.EventsDispatcherOwner
import dev.icerock.moko.resources.desc.desc
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

// todo translate data
class PinCodeViewModel constructor(
    override val eventsDispatcher: EventsDispatcher<EventListener>,
    private val getPinCodeUseCase: GetPinCodeUseCase,
    private val savePinCodeUseCase: SavePinCodeUseCase,
    private val isPinCodeValidUseCase: IsPinCodeValidUseCase,
) : BaseViewModel(), EventsDispatcherOwner<PinCodeViewModel.EventListener> {

    val biometryAuthenticator = BiometryAuthenticator()

    private val _pinCodeState = MutableStateFlow<PinCodeState?>(null)
    val pinCodeState: StateFlow<PinCodeState?> = _pinCodeState

    fun tryToAuth() {
        viewModelScope.launch {
            try {
                val isSuccess = biometryAuthenticator.checkBiometryAuthentication(
                    "Just for test".desc(),
                    "Oops".desc()
                )
                if (isSuccess) {
                    eventsDispatcher.dispatchEvent { onSuccess() }
                }
            } catch (e: Throwable) {
                eventsDispatcher.dispatchEvent { onFail() }
            }
        }
    }

    fun checkTouchIdEnable() {
        if (biometryAuthenticator.isTouchIdEnabled()) {
            _pinCodeState.value = PinCodeState.TouchIdEnable
        }
    }

    fun checkFaceIdEnable() {
        if (biometryAuthenticator.isFaceIdEnabled()) {
            _pinCodeState.value = PinCodeState.FaceIdEnable
        }
    }

    fun savePinCode(pinCode: String) {
        viewModelScope.launch {
            savePinCodeUseCase.savePinCode(pinCode = pinCode)
            _pinCodeState.emit(PinCodeState.Saved)
        }
    }

    fun getPinCode() {
        viewModelScope.launch {
            val pinCode = getPinCodeUseCase.getPinCode()
            _pinCodeState.emit(PinCodeState.GetPinCode(pinCode = pinCode))
        }
    }

    fun comparePinCodes(
        pinCode: String,
        decryption: (String?) -> String
    ) {
        viewModelScope.launch {
            val savedPinCode = decryption.invoke(getPinCodeUseCase.getPinCode())
            println("is_pin_valid_vm: saved pin: $savedPinCode ---- entered: $pinCode")
            val result = isPinCodeValidUseCase.comparePinCodes(pinCode)
            if (result) {
                _pinCodeState.emit(PinCodeState.Success)
            } else {
                // todo show error
            }
        }
    }

    interface EventListener {
        fun onSuccess()
        fun onFail()
    }
}