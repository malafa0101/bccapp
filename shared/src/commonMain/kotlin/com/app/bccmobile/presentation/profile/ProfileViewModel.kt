package com.app.bccmobile.presentation.profile

import com.app.bccapp.domain.model.Response
import com.app.bccmobile.domain.use_case.GetUserUseCase
import com.app.bccmobile.presentation.BaseViewModel
import com.app.bccmobile.presentation.state.LoadingState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class ProfileViewModel constructor(
    private val getUserUseCase: GetUserUseCase
): BaseViewModel() {

    private val _profileState = MutableStateFlow<ProfileState?>(null)
    val profileState: StateFlow<ProfileState?> = _profileState

    fun getUser() {
        viewModelScope.launch {
            _loadingState.emit(LoadingState.Show)
            val result = getUserUseCase.getUser()
            when(result) {
                is Response.Success -> {
                    println("profile_vm: ${result.data}")
                    _profileState.emit(ProfileState.Success(result.data))
                }
                is Response.Error -> {
                    println("profile_vm: ${result.error}")
                    _errorState.emit(result.error)
                }
            }
            _loadingState.emit(LoadingState.Hide)
        }
    }
}