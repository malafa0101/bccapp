package com.app.bccmobile.presentation.otp

import com.app.bccapp.domain.model.Response
import com.app.bccmobile.domain.use_case.SendOtpUseCase
import com.app.bccmobile.presentation.BaseViewModel
import com.app.bccmobile.presentation.state.LoadingState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class OtpViewModel constructor(
    private val sendOtpUseCase: SendOtpUseCase
) : BaseViewModel() {

    private val _otpState = MutableStateFlow<OtpState?>(null)
    val otpState: StateFlow<OtpState?> = _otpState

    fun sendOpt(phone: String) = viewModelScope.launch {
        _loadingState.emit(LoadingState.Show)
        val result = sendOtpUseCase.sendOtp(phone = phone)
        when(result) {
            is Response.Success -> {
                if (result.data) {
                    _otpState.value = OtpState.Success
                }
            }
            is Response.Error -> {
                _errorState.emit(result.error)
            }
        }
        _otpState.value = null
        _loadingState.emit(LoadingState.Hide)
    }
}