package com.app.bccmobile.presentation

import com.app.bccmobile.presentation.state.LoadingState
import dev.icerock.moko.mvvm.viewmodel.ViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow

abstract class BaseViewModel : ViewModel() {

    protected val _loadingState = MutableSharedFlow<LoadingState>(1)
    val loadingState: SharedFlow<LoadingState> = _loadingState

    protected val _errorState = MutableSharedFlow<String?>(1)
    val errorState: SharedFlow<String?> = _errorState

}