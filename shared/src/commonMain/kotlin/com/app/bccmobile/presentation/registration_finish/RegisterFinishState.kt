package com.app.bccmobile.presentation.registration_finish

sealed class RegisterFinishState {
    object Success : RegisterFinishState()
    data class ResetPassword(val message: String) : RegisterFinishState()
}