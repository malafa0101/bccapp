package com.app.bccmobile.presentation.pincode

sealed class PinCodeState {

    object TouchIdEnable : PinCodeState()
    object FaceIdEnable : PinCodeState()
    object Saved : PinCodeState()
    object Success : PinCodeState()

    data class GetPinCode(val pinCode: String?) : PinCodeState()
}
