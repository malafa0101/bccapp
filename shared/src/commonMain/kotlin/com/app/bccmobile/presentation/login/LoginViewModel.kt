package com.app.bccmobile.presentation.login

import com.app.bccapp.domain.model.Response
import com.app.bccmobile.domain.use_case.LoginUseCase
import com.app.bccmobile.presentation.BaseViewModel
import com.app.bccmobile.presentation.state.LoadingState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class LoginViewModel constructor(
    private val loginUseCase: LoginUseCase
) : BaseViewModel() {

    private val _loginState = MutableStateFlow<LoginState?>(null)
    val loginState: StateFlow<LoginState?> = _loginState

    fun login(phone: String, password: String) {
        viewModelScope.launch {
            _loadingState.emit(LoadingState.Show)
            val result = loginUseCase.login(
                params = LoginUseCase.Params(
                    phone = phone,
                    password = password
                )
            )
            println("login_vm: ${result}")
            when(result) {
                is Response.Success -> _loginState.emit(LoginState.Success)
                is Response.Error -> _errorState.emit(result.error)
            }
            _loginState.value = null
            _loadingState.emit(LoadingState.Hide)
        }
    }
}