package com.app.bccmobile.data.repository

import com.app.bccmobile.data.storage.DataStorage
import com.app.bccmobile.domain.repository.KeystoreRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext

class KeystoreRepositoryImpl(
    private val dataStorage: DataStorage,
    private val coroutineDispatcher: CoroutineDispatcher
) : KeystoreRepository {

    override suspend fun savePinCode(pinCode: String) = withContext(coroutineDispatcher) {
        dataStorage.pinCode = pinCode
    }

    override suspend fun getPinCode(): String? = withContext(coroutineDispatcher) {
        dataStorage.pinCode
    }

    override suspend fun isPinCodeValid(pinCode: String): Boolean = withContext(coroutineDispatcher) {
        println("is_pin_valid: saved pin in data store: ${dataStorage.pinCode}")
        println("is_pin_valid: pincode: $pinCode ----- data store: ${dataStorage.pinCode}")
        pinCode == dataStorage.pinCode
    }
}