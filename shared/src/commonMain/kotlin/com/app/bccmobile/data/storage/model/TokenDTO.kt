package com.app.bccapp.data.storage.model

import com.app.bccmobile.domain.model.Token
import kotlinx.serialization.Serializable

@Serializable
data class TokenDTO(
    val jwt: String?,
    val refreshToken: String?,
    val refreshTokenExp: String?
) {
    fun toDomain(): Token {
        return Token(
            jwt = jwt,
            refreshToken = refreshToken,
            refreshTokenExp = refreshTokenExp
        )
    }
}