/**
* Auth-API
* Документация по API
*
* OpenAPI spec version: 1.0.0
* 
*
* NOTE: This class is auto generated by the swagger code generator program.
* https://github.com/swagger-api/swagger-codegen.git
* Do not edit the class manually.
*/
package com.app.bccapp.data.network.model


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


/**
 * 
 * @param isSent 
 * @param otpResendTime 
 */
@Serializable
data class SendOTPResponse (
    
    @SerialName("is_sent")
    val isSent: kotlin.Boolean? = null,
    
    @SerialName("otp_resend_time")
    val otpResendTime: kotlin.String? = null

) {

}

