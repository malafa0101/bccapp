package com.app.bccmobile.data.storage

import com.app.bccapp.data.storage.model.TokenDTO

interface DataStorage {
    var tokenDTO: TokenDTO?

    var pinCode: String?

    companion object {
        const val TOKEN = "token"
        const val PIN_CODE = "pin_code"
    }
}

