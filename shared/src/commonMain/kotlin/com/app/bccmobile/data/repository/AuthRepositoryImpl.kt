package com.app.bccmobile.data.repository

import com.app.bccapp.data.network.model.*
import com.app.bccapp.data.storage.model.TokenDTO
import com.app.bccapp.domain.model.Response
import com.app.bccmobile.JwtTokenDecoder
import com.app.bccmobile.data.network.api.AuthApi
import com.app.bccmobile.data.storage.DataStorage
import com.app.bccmobile.domain.model.Token
import com.app.bccmobile.domain.model.User
import com.app.bccmobile.domain.repository.AuthRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext

class AuthRepositoryImpl constructor(
    private val authApi: AuthApi,
    private val dataStorage: DataStorage,
    private val jwtTokenDecoder: JwtTokenDecoder,
    private val deviceId: String,
    private val coroutineDispatcher: CoroutineDispatcher
): AuthRepository {

    override suspend fun login(
        phone: String,
        password: String
    ): Response<Boolean> = withContext(coroutineDispatcher) {
        try {
            val response = authApi.userAuthenticationRequest(
                body = UserAuthenticationRequest(
                    deviceId = deviceId,
                    mobile =  phone,
                    password = password,
                ),
            )
            dataStorage.tokenDTO = TokenDTO(
                jwt = response.jwt,
                refreshToken = response.refreshToken,
                refreshTokenExp = response.refreshTokenExp
            )
            Response.Success(true)
        } catch (e: Throwable) {
            Response.Error(e.message ?: "")
        }
    }

    override suspend fun refreshJwtToken(): Response<Token> = withContext(coroutineDispatcher) {
        try {
            val refreshToken = dataStorage.tokenDTO?.refreshToken
            val response = authApi.jWTRefreshRequest(
                body = JWTRefreshRequest(
                    refreshToken = refreshToken
                )
            )
            val tokenDTO = TokenDTO(
                jwt = response.jwt,
                refreshToken = response.refreshToken,
                refreshTokenExp = response.refreshTokenExp
            )
            dataStorage.tokenDTO = tokenDTO
            Response.Success(tokenDTO.toDomain())
        } catch (e: Throwable) {
            Response.Error(e.message ?: "")
        }
    }

    override suspend fun getUser(): Response<User> = withContext(coroutineDispatcher) {
        try {
            println("jwt_token_data: ${dataStorage.tokenDTO}")
            val jwt = dataStorage.tokenDTO?.jwt
            val userId = jwtTokenDecoder.decodeUserId(token = jwt) ?: ""
            println("jwt_token_data: decoded: ${userId}")
            val response = authApi.getUserRequest(
                userId = userId,
                authorization = jwt
            )
            Response.Success(response.toDomain())
        } catch (e: Throwable) {
            Response.Error(e.message ?: "")
        }
    }

    override suspend fun linkUserBaseProfile(
        profileId: String
    ): Response<Token> = withContext(coroutineDispatcher) {
        try {
            val response = authApi.linkUserBaseProfileRequest(
                body = LinkUserBaseProfileRequest(
                    deviceId = deviceId,
                    profileId = profileId
                )
            )
            val tokenDTO = TokenDTO(
                jwt = response.jwt,
                refreshToken = response.refreshToken,
                refreshTokenExp = response.refreshTokenExp
            )
            dataStorage.tokenDTO = tokenDTO
            Response.Success(tokenDTO.toDomain())
        } catch (e: Throwable) {
            Response.Error(e.message ?: "")
        }
    }

    override suspend fun passwordResetRequest(
        phone: String,
        otp: String,
        password: String
    ): Response<String> = withContext(coroutineDispatcher) {
        try {
            val response = authApi.passwordResetRequest(
                body = PasswordResetRequest(
                    mobile = phone,
                    otp = otp,
                    password = password
                )
            )
            Response.Success(response.msg ?: "")
        } catch (e: Throwable) {
            Response.Error(e.message ?: "")
        }
    }

    override suspend fun sendOtp(phone: String): Response<Boolean> = withContext(coroutineDispatcher) {
        try {
            val response = authApi.sendOTPRequest(
                body = SendOTPRequest(
                    mobile = phone
                )
            )
            Response.Success(response.isSent ?: false)
        } catch (e: Throwable) {
            Response.Error(e.message ?: "")
        }
    }

    override suspend fun auth(
        phone: String,
        password: String
    ): Response<Token> = withContext(coroutineDispatcher) {
        try {
            val response = authApi.userAuthenticationRequest(
                body = UserAuthenticationRequest(
                    deviceId = deviceId,
                    mobile = phone,
                    password = password
                )
            )
            val tokenDTO = TokenDTO(
                jwt = response.jwt,
                refreshToken = response.refreshToken,
                refreshTokenExp = response.refreshTokenExp
            )
            dataStorage.tokenDTO = tokenDTO
            Response.Success(tokenDTO.toDomain())
        } catch (e: Throwable) {
            Response.Error(e.message ?: "")
        }
    }

    override suspend fun check(phone: String): Response<Boolean> = withContext(coroutineDispatcher) {
        try {
            val response = authApi.userCheckRequest(
                body = UserCheckRequest(
                    mobile = phone
                )
            )
            Response.Success(response.isVerified ?: false)
        } catch (e: Throwable) {
            Response.Error(e.message ?: "")
        }
    }

    override suspend fun registration(
        otp: String,
        phone: String,
        password: String
    ): Response<Boolean> = withContext(coroutineDispatcher) {
        try {
            val response = authApi.userRegistrationRequest(
                body = UserRegistrationRequest(
                    deviceId = deviceId,
                    mobile = phone,
                    password = password,
                    otp = otp
                )
            )
            val tokenDTO = TokenDTO(
                jwt = response.jwt,
                refreshToken = response.refreshToken,
                refreshTokenExp = response.refreshTokenExp
            )
            dataStorage.tokenDTO = tokenDTO
            Response.Success(true)
        } catch (e: Throwable) {
            Response.Error(e.message ?: "")
        }
    }

    override suspend fun resetPassword(
        otp: String,
        phone: String,
        password: String
    ): Response<String> = withContext(coroutineDispatcher) {
        try {
            val response = authApi.passwordResetRequest(
                body = PasswordResetRequest(
                    mobile = phone,
                    otp = otp,
                    password = password
                )
            )
            Response.Success(response.msg ?: "")
        } catch (e: Throwable) {
            Response.Error(e.message ?: "")
        }
    }
}