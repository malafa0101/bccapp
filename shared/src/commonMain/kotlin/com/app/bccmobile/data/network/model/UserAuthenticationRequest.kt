package com.app.bccapp.data.network.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


/**
 * 
 * @param deviceId 
 * @param mobile 
 * @param password 
 */
@Serializable
data class UserAuthenticationRequest (
    
    @SerialName("device_id")
    val deviceId: kotlin.String? = null,
    
    @SerialName("mobile")
    val mobile: kotlin.String? = null,
    
    @SerialName("password")
    val password: kotlin.String? = null

) {

}

