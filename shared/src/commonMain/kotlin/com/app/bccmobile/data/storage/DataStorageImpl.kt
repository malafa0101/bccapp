package com.app.bccmobile.data.storage

import com.app.bccapp.data.storage.model.TokenDTO
import com.russhwolf.settings.Settings
import com.russhwolf.settings.serialization.decodeValueOrNull
import com.russhwolf.settings.serialization.encodeValue

class DataStorageImpl constructor(
    private val settings: Settings
) : DataStorage {

    override var tokenDTO
        get(): TokenDTO? {
            return settings.decodeValueOrNull(TokenDTO.serializer(), DataStorage.TOKEN)
        }
        set(value) {
            if (value != null) {
                settings.encodeValue(TokenDTO.serializer(), DataStorage.TOKEN, value)
            }
        }
    override var pinCode
        get(): String? {
            return settings.getString(DataStorage.PIN_CODE, "")
        }
        set(value) {
            if (!value.isNullOrEmpty()) {
                settings.putString(DataStorage.PIN_CODE, value)
            }
        }
}