package com.app.bccmobile.data.network

import io.ktor.client.*
import io.ktor.client.features.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.features.logging.*

object NetworkClient {

    fun getHttpClient(): HttpClient {
        return HttpClient() {
            install(JsonFeature) {
                val json = kotlinx.serialization.json.Json { ignoreUnknownKeys = true }
                serializer = KotlinxSerializer(json)

            }
            install(Logging) {
                logger = CustomHttpLogger()
                level = LogLevel.ALL
            }
            HttpResponseValidator {
                validateResponse { response ->
                    val statusCode = response.status.value
                    when (statusCode) {
                        in 300..399 -> throw RedirectResponseException(response, "")
                        in 400..499 -> throw ClientRequestException(response, "")
                        in 500..599 -> throw ServerResponseException(response, "")
                    }
                    if (statusCode >= 600) {
                        throw ResponseException(response, "")
                    }
                }
            }
        }
    }

    class CustomHttpLogger() : Logger {
        override fun log(message: String) {
            println("bcc_app: $message")
        }
    }
}