package com.app.bccmobile

expect class JwtTokenDecoder() {
    fun decode(token: String?): String

    fun decodeUserId(token: String?): String?
}