package com.app.bccmobile.di

import com.app.bccmobile.domain.use_case.LoginUseCase
import com.app.bccmobile.presentation.login.LoginViewModel

class LoginFactory constructor(
    private val loginUseCase: LoginUseCase
) {

    fun createViewModel(): LoginViewModel {
        return LoginViewModel(
            loginUseCase = loginUseCase
        )
    }
}