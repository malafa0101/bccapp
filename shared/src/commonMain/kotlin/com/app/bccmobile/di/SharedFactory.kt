package com.app.bccmobile.di

import com.app.bccmobile.JwtTokenDecoder
import com.app.bccmobile.data.network.NetworkClient
import com.app.bccmobile.data.network.api.AuthApi
import com.app.bccmobile.data.network.api.AuthApiImpl
import com.app.bccmobile.data.repository.AuthRepositoryImpl
import com.app.bccmobile.data.repository.KeystoreRepositoryImpl
import com.app.bccmobile.data.storage.DataStorageImpl
import com.app.bccmobile.domain.use_case.*
import com.app.bccmobile.domain.use_case.pincode.GetPinCodeUseCase
import com.app.bccmobile.domain.use_case.pincode.IsPinCodeValidUseCase
import com.app.bccmobile.domain.use_case.pincode.SavePinCodeUseCase
import com.russhwolf.settings.Settings
import io.ktor.client.*
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.serialization.json.Json
import kotlin.random.Random

class SharedFactory constructor(
    private val settings: Settings,
    private val jwtTokenDecoder: JwtTokenDecoder,
    private val baseUrl: String,
    private val tokenDecoder: () -> Unit
) {

    private val coroutineDispatcher: CoroutineDispatcher by lazy {
        Dispatchers.Default
    }

    private val httpClient: HttpClient by lazy {
        NetworkClient.getHttpClient()
    }

    private val json: Json by lazy {
        Json {
            ignoreUnknownKeys = true
        }
    }

    private val authApi: AuthApi by lazy {
        AuthApiImpl(
            basePath = baseUrl,
            httpClient = httpClient,
            json = json
        )
    }

    private val dataStorage by lazy {
        DataStorageImpl(settings = settings)
    }

    // todo this temp solution for deviceId
    private val deviceId by lazy {
        Random(100000000).nextLong().toString()
    }

    private val authRepository by lazy {
        AuthRepositoryImpl(
            authApi = authApi,
            dataStorage = dataStorage,
            jwtTokenDecoder = jwtTokenDecoder,
            deviceId = deviceId,
            coroutineDispatcher = coroutineDispatcher
        )
    }

    private val keystoreRepository by lazy {
        KeystoreRepositoryImpl(
            dataStorage = dataStorage,
            coroutineDispatcher = coroutineDispatcher
        )
    }

    private val loginUseCase by lazy {
        LoginUseCase(authRepository = authRepository)
    }

    private val getUserUseCase by lazy {
        GetUserUseCase(authRepository = authRepository)
    }

    private val refreshTokenUseCase by lazy {
        RefreshTokenUseCase(authRepository = authRepository)
    }

    private val passwordResetUseCase by lazy {
        PasswordResetUseCase(authRepository = authRepository)
    }

    private val sendOtpUseCase by lazy {
        SendOtpUseCase(authRepository = authRepository)
    }

    private val authUseCase by lazy {
        AuthUseCase(authRepository = authRepository)
    }

    private val registerUseCase by lazy {
        RegistrationUseCase(authRepository = authRepository)
    }

    private val checkUserUseCase by lazy {
        CheckUserUseCase(authRepository = authRepository)
    }

    private val resetPasswordUseCase by lazy {
        ResetPasswordUseCase(authRepository = authRepository)
    }

    private val getPinCodeValidUseCase by lazy {
        GetPinCodeUseCase(keystoreRepository = keystoreRepository)
    }

    private val savePinCodeUseCase by lazy {
        SavePinCodeUseCase(keystoreRepository = keystoreRepository)
    }

    private val isPinCodeValidUseCase by lazy {
        IsPinCodeValidUseCase(keystoreRepository = keystoreRepository)
    }

    val loginFactory by lazy {
        LoginFactory(loginUseCase = loginUseCase)
    }

    val otpFactory by lazy {
        OtpFactory(sendOtpUseCase = sendOtpUseCase)
    }

    val registerFactory by lazy {
        RegisterFactory(
            checkUserUseCase = checkUserUseCase,
            sendOtpUseCase = sendOtpUseCase
        )
    }

    val registrationFinishFactory by lazy {
        RegistrationFinishFactory(
            registerUseCase = registerUseCase,
            resetPasswordUseCase = resetPasswordUseCase
        )
    }

    val pinCodeFactory by lazy {
        PinCodeFactory(
            getPinCodeUseCase = getPinCodeValidUseCase,
            savePinCodeUseCase = savePinCodeUseCase,
            isPinCodeValidUseCase = isPinCodeValidUseCase
        )
    }

    val profileFactory by lazy {
        ProfileFactory(getUserUseCase = getUserUseCase)
    }
}