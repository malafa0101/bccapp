package com.app.bccmobile.di

import com.app.bccmobile.domain.use_case.CheckUserUseCase
import com.app.bccmobile.domain.use_case.SendOtpUseCase
import com.app.bccmobile.presentation.registration.RegisterViewModel

class RegisterFactory constructor(
    private val checkUserUseCase: CheckUserUseCase,
    private val sendOtpUseCase: SendOtpUseCase
) {

    fun createViewModel(): RegisterViewModel {
        return RegisterViewModel(
            checkUserUseCase = checkUserUseCase,
            sendOtpUseCase = sendOtpUseCase
        )
    }
}