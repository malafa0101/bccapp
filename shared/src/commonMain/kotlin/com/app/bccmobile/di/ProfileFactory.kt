package com.app.bccmobile.di

import com.app.bccmobile.domain.use_case.GetUserUseCase
import com.app.bccmobile.presentation.profile.ProfileViewModel

class ProfileFactory constructor(
    private val getUserUseCase: GetUserUseCase
) {

    fun createViewModel(): ProfileViewModel {
        return ProfileViewModel(getUserUseCase = getUserUseCase)
    }
}