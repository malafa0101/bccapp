package com.app.bccmobile.di

import com.app.bccmobile.domain.use_case.pincode.GetPinCodeUseCase
import com.app.bccmobile.domain.use_case.pincode.IsPinCodeValidUseCase
import com.app.bccmobile.domain.use_case.pincode.SavePinCodeUseCase
import com.app.bccmobile.presentation.pincode.PinCodeViewModel
import dev.icerock.moko.mvvm.dispatcher.EventsDispatcher

class PinCodeFactory constructor(
    private val getPinCodeUseCase: GetPinCodeUseCase,
    private val savePinCodeUseCase: SavePinCodeUseCase,
    private val isPinCodeValidUseCase: IsPinCodeValidUseCase
) {

    fun createViewModel(
        eventsDispatcher: EventsDispatcher<PinCodeViewModel.EventListener>,
    ): PinCodeViewModel {
        return PinCodeViewModel(
            getPinCodeUseCase = getPinCodeUseCase,
            savePinCodeUseCase = savePinCodeUseCase,
            isPinCodeValidUseCase = isPinCodeValidUseCase,
            eventsDispatcher = eventsDispatcher
        )
    }
}