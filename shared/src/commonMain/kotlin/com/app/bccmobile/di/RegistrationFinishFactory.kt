package com.app.bccmobile.di

import com.app.bccmobile.domain.use_case.RegistrationUseCase
import com.app.bccmobile.domain.use_case.ResetPasswordUseCase
import com.app.bccmobile.presentation.registration_finish.RegistrationFinishViewModel

class RegistrationFinishFactory constructor(
    private val registerUseCase: RegistrationUseCase,
    private val resetPasswordUseCase: ResetPasswordUseCase
) {

    fun createViewModel(): RegistrationFinishViewModel {
        return RegistrationFinishViewModel(
            registrationUseCase = registerUseCase,
            resetPasswordUseCase = resetPasswordUseCase
        )
    }
}