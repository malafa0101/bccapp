package com.app.bccmobile.di

import com.app.bccmobile.domain.use_case.SendOtpUseCase
import com.app.bccmobile.presentation.otp.OtpViewModel

class OtpFactory constructor(
    private val sendOtpUseCase: SendOtpUseCase
) {

    fun createViewModel(): OtpViewModel {
        return OtpViewModel(
            sendOtpUseCase = sendOtpUseCase
        )
    }
}