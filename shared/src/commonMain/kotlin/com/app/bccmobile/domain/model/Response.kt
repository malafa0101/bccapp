package com.app.bccapp.domain.model

import kotlinx.serialization.Serializable

sealed class Response<out T> {
    data class Success<out T>(val data: T): Response<T>()
    data class Error(val error: String) : Response<Nothing>()
}

@Serializable
data class ErrorResponse(
    val errorCode: Int? = 401,   //if by errorCode you mean the http status code is not really necessary to include here as you already know it from the validateResponse
    val errorMessage: String
)