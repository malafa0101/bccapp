package com.app.bccmobile.domain.use_case

import com.app.bccmobile.domain.repository.AuthRepository

class ResetPasswordUseCase constructor(
    private val authRepository: AuthRepository
) {

    suspend fun resetPassword(
        param: Param
    ) = authRepository.resetPassword(
        phone = param.phone,
        otp = param.otp,
        password = param.password
    )

    data class Param(
        val phone: String,
        val otp: String,
        val password: String
    )
}