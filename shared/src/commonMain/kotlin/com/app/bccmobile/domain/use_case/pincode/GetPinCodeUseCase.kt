package com.app.bccmobile.domain.use_case.pincode

import com.app.bccmobile.domain.repository.KeystoreRepository

class GetPinCodeUseCase constructor(
    private val keystoreRepository: KeystoreRepository
) {

    suspend fun getPinCode() = keystoreRepository.getPinCode()
}