package com.app.bccmobile.domain.use_case

import com.app.bccmobile.domain.repository.AuthRepository

class LoginUseCase constructor(
    private val authRepository: AuthRepository
) {
    suspend fun login(params: Params) =
        authRepository.login(phone = params.phone, password = params.password)

    data class Params(
        val phone: String,
        val password: String
    )
}