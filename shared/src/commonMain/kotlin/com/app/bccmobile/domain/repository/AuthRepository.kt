package com.app.bccmobile.domain.repository

import com.app.bccapp.domain.model.Response
import com.app.bccmobile.domain.model.Token
import com.app.bccmobile.domain.model.User

interface AuthRepository {

    suspend fun login(phone: String, password: String): Response<Boolean>

    suspend fun refreshJwtToken(): Response<Token>

    suspend fun getUser(): Response<User>

    suspend fun linkUserBaseProfile(profileId: String): Response<Token>

    suspend fun passwordResetRequest(
        phone: String,
        otp: String,
        password: String
    ): Response<String>

    suspend fun sendOtp(phone: String): Response<Boolean>

    suspend fun auth(phone: String, password: String): Response<Token>

    suspend fun check(phone: String): Response<Boolean>

    suspend fun registration(
        otp: String,
        phone: String,
        password: String
    ): Response<Boolean>

    suspend fun resetPassword(
        otp: String,
        phone: String,
        password: String
    ): Response<String>
}