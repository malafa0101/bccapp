package com.app.bccmobile.domain.use_case.pincode

import com.app.bccmobile.domain.repository.KeystoreRepository

class IsPinCodeValidUseCase constructor(
    private val keystoreRepository: KeystoreRepository
) {

    suspend fun comparePinCodes(pinCode: String) = keystoreRepository.isPinCodeValid(pinCode)
}