package com.app.bccmobile.domain.use_case

import com.app.bccmobile.domain.repository.AuthRepository

class AuthUseCase constructor(
    private val authRepository: AuthRepository
) {

    suspend fun auth(params: Params) =
        authRepository.auth(
            password = params.password,
            phone = params.phone
        )

    data class Params(
        val phone: String,
        val password: String
    )
}