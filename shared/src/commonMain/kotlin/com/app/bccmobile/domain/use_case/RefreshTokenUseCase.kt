package com.app.bccmobile.domain.use_case

import com.app.bccmobile.domain.repository.AuthRepository

class RefreshTokenUseCase constructor(
    private val authRepository: AuthRepository
) {

    suspend fun refreshJwtToken() = authRepository.refreshJwtToken()
}