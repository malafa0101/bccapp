package com.app.bccmobile.domain.use_case

import com.app.bccmobile.domain.repository.AuthRepository

class PasswordResetUseCase constructor(
    private val authRepository: AuthRepository
) {

    suspend fun passwordReset(params: Params) = authRepository.passwordResetRequest(
        phone = params.phone,
        otp = params.otp,
        password = params.password
    )

    data class Params(
        val phone: String,
        val otp: String,
        val password: String
    )
}