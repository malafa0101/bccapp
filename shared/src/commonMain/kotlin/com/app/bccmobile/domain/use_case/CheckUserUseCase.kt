package com.app.bccmobile.domain.use_case

import com.app.bccmobile.domain.repository.AuthRepository

class CheckUserUseCase constructor(
    private val authRepository: AuthRepository
) {

    suspend fun checkUser(phone: String) = authRepository.check(phone = phone)
}