package com.app.bccmobile.domain.model

data class User(
    val baseProfileId: String? = null,
    val createdAt: String? = null,
    val id: String? = null,
    val isBanned: Boolean? = null,
    val isSuperAdmin: Boolean? = null,
    val isVerified: Boolean? = null,
    val mobile: String? = null,
    val updatedAt: String? = null
)
