package com.app.bccmobile.domain.use_case

import com.app.bccmobile.domain.repository.AuthRepository

class GetUserUseCase constructor(
    private val authRepository: AuthRepository
) {

    suspend fun getUser() = authRepository.getUser()
}