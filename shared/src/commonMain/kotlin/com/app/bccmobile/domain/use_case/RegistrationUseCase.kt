package com.app.bccmobile.domain.use_case

import com.app.bccmobile.domain.repository.AuthRepository

class RegistrationUseCase constructor(
    private val authRepository: AuthRepository
) {

    suspend fun register(param: Param) = authRepository.registration(
        otp = param.otp,
        phone = param.phone,
        password = param.password
    )

    data class Param(
        val otp: String,
        val phone: String,
        val password: String
    )
}