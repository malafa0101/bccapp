package com.app.bccmobile.domain.repository

interface KeystoreRepository {
    suspend fun savePinCode(pinCode: String)
    suspend fun getPinCode(): String?
    suspend fun isPinCodeValid(pinCode: String): Boolean
}