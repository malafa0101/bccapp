package com.app.bccmobile.domain.use_case.pincode

import com.app.bccmobile.domain.repository.KeystoreRepository

class SavePinCodeUseCase constructor(
    private val keystoreRepository: KeystoreRepository
) {

    suspend fun savePinCode(pinCode: String) = keystoreRepository.savePinCode(pinCode)
}