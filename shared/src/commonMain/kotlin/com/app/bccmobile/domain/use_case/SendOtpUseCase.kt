package com.app.bccmobile.domain.use_case

import com.app.bccmobile.domain.repository.AuthRepository

class SendOtpUseCase constructor(
    private val authRepository: AuthRepository
) {

    suspend fun sendOtp(phone: String) = authRepository.sendOtp(phone = phone)
}