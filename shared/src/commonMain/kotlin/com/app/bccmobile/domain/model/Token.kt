package com.app.bccmobile.domain.model

data class Token(
    val jwt: String?,
    val refreshToken: String?,
    val refreshTokenExp: String?
)