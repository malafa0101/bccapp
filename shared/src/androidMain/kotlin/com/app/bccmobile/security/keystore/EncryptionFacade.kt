package com.app.bccmobile.security.keystore

import android.content.Context
import dev.icerock.moko.biometry.BiometryAuthenticator

class EncryptionFacade(context: Context) {

    private val keyStoreWrapper = KeyStoreWrapper(context)

    private val encryptionServices = EncryptionServices(keyStoreWrapper)

    fun createMasterKey(
        biometryAuthenticator: BiometryAuthenticator,
        password: String
    ) {
        encryptionServices.createMasterKey(password = password)
        createKey(biometryAuthenticator)
    }

    fun encrypt(value: String): String {
        return encryptionServices.encrypt(value)
    }

    fun decrypt(value: String): String {
        return encryptionServices.decrypt(value)
    }

    private fun createKey(biometryAuthenticator: BiometryAuthenticator) {
        if (biometryAuthenticator.isTouchIdEnabled()) {
            encryptionServices.createFingerprintKey()
        }
        encryptionServices.createConfirmCredentialsKey()
    }
}