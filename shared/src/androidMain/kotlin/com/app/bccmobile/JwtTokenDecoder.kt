package com.app.bccmobile

import com.auth0.android.jwt.JWT


actual class JwtTokenDecoder actual constructor() {

    actual fun decode(token: String?): String {
        if (token.isNullOrEmpty()) {
            return ""
        }
        val jwt = JWT(token)
        return jwt.claims.values.first().asString() ?: ""
    }

    actual fun decodeUserId(token: String?): String? {
        if (token.isNullOrEmpty()) {
            return null
        }
        val jwt = JWT(token)
        return jwt.claims.get("user_id")?.asString()
    }
}