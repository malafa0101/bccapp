import org.jetbrains.kotlin.gradle.plugin.mpp.KotlinNativeTarget

plugins {
    kotlin("multiplatform")
    kotlin("native.cocoapods")
    id("com.android.library")
    id("kotlinx-serialization")
}

version = "1.0"

repositories {
    jcenter()
    maven { url = uri("https://jitpack.io") }
}

kotlin {
    android()

    val iosTarget: (String, KotlinNativeTarget.() -> Unit) -> KotlinNativeTarget =
        if (System.getenv("SDK_NAME")?.startsWith("iphoneos") == true)
            ::iosArm64
        else
            ::iosX64

    iosTarget("ios") {}

    cocoapods {
        summary = "Some description for the Shared Module"
        homepage = "Link to the Shared Module homepage"
        ios.deploymentTarget = "14.1"
        frameworkName = "shared"
        podfile = project.file("../iosApp/Podfile")
    }

    val ktor_version = "1.5.0"
    val coroutines_version = "1.4.2"
    val koin_version = "3.0.2"
    val settings_version = "0.7.7"

    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation("io.ktor:ktor-client-core:${ktor_version}")
                implementation("io.ktor:ktor-client-serialization:${ktor_version}")
                implementation("io.ktor:ktor-client-logging:${ktor_version}")

                implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.2.1")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.4.2-native-mt")

                implementation("com.russhwolf:multiplatform-settings:$settings_version")
                implementation("com.russhwolf:multiplatform-settings-no-arg:$settings_version")
                implementation("com.russhwolf:multiplatform-settings-serialization:$settings_version")

                implementation("dev.icerock.moko:mvvm-core:0.10.1")
                api("dev.icerock.moko:biometry:0.1.1")
            }
        }
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test-common"))
                implementation(kotlin("test-annotations-common"))
            }
        }
        val androidMain by getting {
            dependencies {
                implementation("io.ktor:ktor-client-android:$ktor_version")
                implementation("dev.icerock.moko:mvvm-viewbinding:0.10.1")
                implementation("dev.icerock.moko:mvvm-livedata-material:0.10.1")

                implementation("com.auth0.android:jwtdecode:2.0.0")

                implementation ("androidx.biometric:biometric:1.1.0")
            }
        }
        val androidTest by getting {
            dependencies {
                implementation(kotlin("test-junit"))
                implementation("junit:junit:4.13.2")
            }
        }
        val iosMain by getting
        val iosTest by getting
    }
}

android {
    compileSdkVersion(30)
    sourceSets["main"].manifest.srcFile("src/androidMain/AndroidManifest.xml")
    defaultConfig {
        minSdkVersion(21)
        targetSdkVersion(30)
    }
}