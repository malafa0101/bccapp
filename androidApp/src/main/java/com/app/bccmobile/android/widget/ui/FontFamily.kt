package com.app.bccmobile.android.widget.ui

import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import com.app.bccmobile.android.R

val sf_pro_display_family = FontFamily(
    Font(R.font.sf_pro_display, FontWeight.Normal),
    Font(R.font.sf_pro_display_medium, FontWeight.Medium),
)
val axiforma_family = FontFamily(
    Font(R.font.axiforma_semi_bold, FontWeight.SemiBold)
)