package com.app.bccmobile.android.feature.auth.pincode

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.app.bccmobile.android.R
import com.app.bccmobile.android.widget.compose.PinButtonsGroup
import com.app.bccmobile.android.widget.compose.PinCodeWidget
import com.app.bccmobile.android.widget.compose.SpacingVertical
import com.app.bccmobile.presentation.pincode.PinCodeState

// todo translate
@Composable
fun PinCodeScreen(
    pinCodeScreenState: PinCodeScreenState,
    pinCodeLength: Int = 4,
    touchIdState: PinCodeState?,
    onFingerprintClick: () -> Unit,
    onBackClick: () -> Unit,
    onLogoutClick: () -> Unit,
    onPinCodeValid: (String) -> Unit,
    onPinCodeCreated: (String) -> Unit,
) {

    val firstInputState = remember {
        mutableStateOf("")
    }
    val secondInputState = remember {
        mutableStateOf("")
    }

    val firstPinEntered = remember {
        mutableStateOf(false)
    }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(top = 20.dp, bottom = 16.dp)
    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Row(
                horizontalArrangement = Arrangement.Start,
                verticalAlignment = Alignment.CenterVertically
            ) {
                IconButton(
                    onClick = onBackClick,
                ) {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_close),
                        contentDescription = "",
                        tint = colorResource(id = R.color.grayA9ACB0)
                    )
                }
            }
            Row(
                horizontalArrangement = Arrangement.End,
                verticalAlignment = Alignment.CenterVertically
            ) {
                IconButton(
                    onClick = onLogoutClick,
                ) {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_log_out),
                        contentDescription = "",
                        tint = colorResource(id = R.color.grayA9ACB0)
                    )
                }
            }
        }

        Box(
            contentAlignment = Alignment.Center,
            modifier = Modifier.fillMaxWidth(),
        ) {
            Image(
                painter = painterResource(id = R.drawable.ic_facebook),
                contentDescription = "",
                modifier = Modifier
                    .width(128.dp)
                    .height(128.dp)
                    .clip(shape = CircleShape)
                    .background(color = Color.Gray),
            )
        }

        Box(
            modifier = Modifier.fillMaxWidth(),
            contentAlignment = Alignment.Center
        ) {
            Text(
                text = "Алина Пак, здравствуйте!",
                modifier = Modifier
                    .fillMaxWidth()
                    .align(Alignment.Center)
                    .padding(top = 24.dp),
                textAlign = TextAlign.Center,
                fontSize = 16.sp
            )
        }
        SpacingVertical(heightDp = 32)
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(
                    start = 96.dp,
                    end = 96.dp
                )
        ) {
            PinCodeWidget(
                pinCodeLength = pinCodeLength,
                pinInputState = firstInputState
            ) { pin ->
                if (pinCodeScreenState == PinCodeScreenState.ENTER) {
                    if (pin.length == pinCodeLength) {
                        onPinCodeValid.invoke(pin)
                    }
                } else {
                    firstPinEntered.value = pin.length >= pinCodeLength
                }
            }
            SpacingVertical(heightDp = 16)
            if (firstPinEntered.value && pinCodeScreenState != PinCodeScreenState.ENTER) {
                PinCodeWidget(
                    pinCodeLength = pinCodeLength,
                    pinInputState = secondInputState
                ) { secondPin ->
                    val firstPin = firstInputState.value
                    if (secondPin.length == pinCodeLength) {
                        if (secondPin == firstPin) {
                            onPinCodeCreated.invoke(firstPin)
                        } else {
                            firstInputState.value = ""
                            secondInputState.value = ""
                            firstPinEntered.value = false
                        }
                    }
                }
            }
        }
        SpacingVertical(heightDp = 16)
        Box(
            modifier = Modifier.fillMaxWidth(),
            contentAlignment = Alignment.Center
        ) {
            Text(
                text = "Повторите пин-код для быстрого доступа к приложению",
                modifier = Modifier
                    .fillMaxWidth()
                    .align(Alignment.Center),
                textAlign = TextAlign.Center,
                fontSize = 14.sp
            )
        }

        PinButtonsGroup(
            pinInputState = if (firstPinEntered.value) secondInputState else firstInputState,
            onFingerprintClick = onFingerprintClick,
            touchIdEnable = touchIdState == PinCodeState.TouchIdEnable
        )
    }
}