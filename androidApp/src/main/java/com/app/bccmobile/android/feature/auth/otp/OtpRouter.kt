package com.app.bccmobile.android.feature.auth.otp

import android.util.Log
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.app.bccmobile.android.R
import com.app.bccmobile.android.feature.auth.register_finish.RegisterFinishFragment

interface OtpRouter {

    fun exit()

    fun routeToRegisterFinish(phone: String, otp: String, resetPassword: Boolean)
}

class OtpRouterImpl(
    private val fragment: Fragment
) : OtpRouter {

    override fun exit() {
        val result = fragment.findNavController().navigateUp()
        Log.d("otp_route", "exit: $result")
    }

    override fun routeToRegisterFinish(
        phone: String,
        otp: String,
        resetPassword: Boolean
    ) {
        fragment.findNavController()
            .navigate(
                R.id.action_otpFragment_to_registerFinishFragment,
                bundleOf(
                    RegisterFinishFragment.OTP to otp,
                    RegisterFinishFragment.PHONE to phone,
                    RegisterFinishFragment.RESET_PASSWORD to resetPassword
                )
            )
    }

}