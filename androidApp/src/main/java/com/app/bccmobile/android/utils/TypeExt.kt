package com.app.bccmobile.android.utils

import android.content.res.Resources
import android.util.DisplayMetrics
import android.util.TypedValue


fun Int.toDp(): Int = (this * (Resources.getSystem().displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)).toInt()

fun Int.toPx(): Int = (this * Resources.getSystem().displayMetrics.densityDpi.toFloat()).toInt()

fun Int.toSp(): Int = TypedValue.applyDimension(
    TypedValue.COMPLEX_UNIT_SP,
    this.toFloat(),
    Resources.getSystem().displayMetrics
).toInt()

fun Float.toDp(): Float = this / Resources.getSystem().displayMetrics.densityDpi.toFloat()

fun Float.toPx(): Float = this * Resources.getSystem().displayMetrics.densityDpi.toFloat()

fun Float.toSp(): Int = TypedValue.applyDimension(
    TypedValue.COMPLEX_UNIT_SP,
    this,
    Resources.getSystem().displayMetrics
).toInt()

fun Float.toSpFloat(): Float = TypedValue.applyDimension(
    TypedValue.COMPLEX_UNIT_SP,
    this,
    Resources.getSystem().displayMetrics
)

fun Double.toDp(): Double = this / Resources.getSystem().displayMetrics.densityDpi.toDouble()

fun Double.toPx(): Double = this * Resources.getSystem().displayMetrics.densityDpi.toDouble()

fun Double.toSp(): Int = TypedValue.applyDimension(
    TypedValue.COMPLEX_UNIT_SP,
    this.toFloat(),
    Resources.getSystem().displayMetrics
).toInt()
