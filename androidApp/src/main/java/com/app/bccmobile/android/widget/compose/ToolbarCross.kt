package com.app.bccmobile.android.widget.compose

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.app.bccmobile.android.R

@Composable
fun ToolbarCross(
    text: String,
    onBackClick: () -> Unit
) {
    Box(modifier = Modifier.fillMaxWidth()) {
        Text(
            text = text,
            maxLines = 1,
            style = MaterialTheme.typography.body1,
            color = colorResource(id = R.color.gray3A3A3F),
            modifier = Modifier.align(Alignment.Center),
            textAlign = TextAlign.Center,
            fontSize = 18.sp
        )

        IconButton(
            onClick = onBackClick,
            modifier = Modifier.align(Alignment.TopEnd).padding(end = 15.dp)
        ) {
            Icon(
                painter = painterResource(id = R.drawable.ic_close),
                contentDescription = "",
                tint = colorResource(id = R.color.grayA9ACB0)
            )
        }
    }
}