package com.app.bccmobile.android.utils

import com.redmadrobot.inputmask.helper.Mask
import com.redmadrobot.inputmask.model.CaretString

fun String.formatPhone(mask: String = PhoneMask.MASK_1): String {
    val result = Mask(mask)
        .apply(
            CaretString(
                string = this,
                caretPosition = this.length,
                caretGravity = CaretString.CaretGravity.FORWARD(false)
            )
        )
    return result.formattedText.string
}

fun String.formattedPhoneIsValid(
    mask: String = PhoneMask.MASK_1,
    length: Int = 11
): Boolean {
    val result = Mask(mask)
        .apply(
            CaretString(
                string = this,
                caretPosition = this.length,
                caretGravity = CaretString.CaretGravity.FORWARD(false)
            )
        )
    return result.formattedText.string.length >= length
}