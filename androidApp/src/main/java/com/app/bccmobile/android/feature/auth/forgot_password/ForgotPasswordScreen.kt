package com.app.bccmobile.android.feature.auth.forgot_password

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.app.bccmobile.android.R
import com.app.bccmobile.android.utils.formattedPhoneIsValid
import com.app.bccmobile.android.widget.compose.*

// todo translate
@Composable
fun ForgotPasswordScreen(
    onRestorePasswordClick: (String) -> Unit,
    onRegisterClick: () -> Unit,
    onBackClick: () -> Unit
) {
    val phone = remember { mutableStateOf("") }
    val isButtonEnable: Boolean = remember(phone.value) {
        val phoneValue = phone.value.trim()
        val phoneIsValid = phoneValue.isNotEmpty() && phoneValue.formattedPhoneIsValid()
        phoneIsValid
    }

    Column(
        modifier = Modifier.fillMaxSize()
    ) {
        Toolbar(
            text = stringResource(id = R.string.login_toolbar_title),
            onBackClick = onBackClick
        )
        Column(
            modifier = Modifier
                .fillMaxSize()
                .absolutePadding(
                    left = 16.dp,
                    right = 16.dp
                )
                .background(Color.White)
                .verticalScroll(rememberScrollState()),
        ) {
            SpacingVertical(80)
            ImageHeader()
            Row(
                modifier = Modifier.absolutePadding(
                    top = 24.dp
                )
            ) {
                Text(
                    text = stringResource(id = R.string.no_account),
                    modifier = Modifier.absolutePadding(
                        left = 20.dp
                    )
                )
                Text(
                    text = stringResource(id = R.string.register),
                    modifier = Modifier
                        .absolutePadding(
                            left = 16.dp
                        )
                        .noRippleClickable(onClick = onRegisterClick),
                    color = colorResource(id = R.color.green27AE60),
                    fontSize = 15.sp
                )
            }
            Text(
                text = "Введите ваш номер",
                modifier = Modifier
                    .fillMaxWidth()
                    .absolutePadding(
                        left = 20.dp,
                        right = 20.dp,
                        top = 24.dp,
                    )
            )
            PhoneNumberInput(numberState = phone)
            MainButton(
                text = stringResource(id = R.string.restore),
                enableState = isButtonEnable,
                onClick = {
                    onRestorePasswordClick.invoke(phone.value)
                },
            )
            SpacingVertical(20)
        }
    }
}