package com.app.bccmobile.android.utils

import android.view.inputmethod.EditorInfo
import android.widget.Button
import android.widget.EditText
import com.redmadrobot.inputmask.MaskedTextChangedListener

fun EditText.isPhoneFilled() = text?.length == 18

fun EditText.setPhoneMask() {
    val listener = MaskedTextChangedListener (PhoneMask.MASK_2, this)
    this.addTextChangedListener(listener)
    this.onFocusChangeListener = listener
}

fun EditText.onDone(btn: Button, imeAction: Int = EditorInfo.IME_ACTION_DONE) {
    setOnEditorActionListener { _, actionId, _ ->
        if (actionId == imeAction && btn.isEnabled) {
            btn.callOnClick()
            return@setOnEditorActionListener true
        }
        false
    }
}