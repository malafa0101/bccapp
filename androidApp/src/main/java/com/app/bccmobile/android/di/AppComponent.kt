/*
 * Copyright 2019 IceRock MAG Inc. Use of this source code is governed by the Apache 2.0 license.
 */

package com.app.bccmobile.android.di

import com.app.bccmobile.di.SharedFactory

object AppComponent {
    lateinit var factory: SharedFactory
}
