package com.app.bccmobile.android

import android.app.Application
import android.content.Context
import com.app.bccmobile.JwtTokenDecoder
import com.app.bccmobile.android.di.AppComponent
import com.app.bccmobile.di.SharedFactory
import com.russhwolf.settings.AndroidSettings

class BccApp : Application() {

    override fun onCreate() {
        super.onCreate()
        AppComponent.factory = SharedFactory(
            settings = AndroidSettings(getSharedPreferences("app", Context.MODE_PRIVATE)),
            jwtTokenDecoder = JwtTokenDecoder(),
            baseUrl = BuildConfig.BASE_URL,
            tokenDecoder = {

            }
        )
    }
}