package com.app.bccmobile.android.feature.auth.register_finish

import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.app.bccmobile.android.R


interface RegisterFinishRouter {

    fun routeBack()

    fun routeToMenu()

    fun routeToLogin()
}

class RegisterFinishRouterImpl(
    private val fragment: Fragment
) : RegisterFinishRouter {

    override fun routeBack() {
        fragment.findNavController().navigateUp()
    }

    override fun routeToMenu() {
        fragment.findNavController()
            .navigate(R.id.action_registerFinishFragment_to_nav_menu)
    }

    override fun routeToLogin() {
        fragment.findNavController()
            .navigate(R.id.action_registerFinishFragment_to_registerFragment)
    }

}