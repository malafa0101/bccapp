package com.app.bccmobile.android.feature.auth.register_finish

import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.absolutePadding
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.app.bccmobile.android.R
import com.app.bccmobile.android.widget.compose.*

// todo translate
@Composable
fun RegistrationFinishScreen(
    onRegisterClick: (String) -> Unit,
    onBackClick: () -> Unit
) {

    val password = remember { mutableStateOf("") }
    val passwordVisibility = remember { mutableStateOf(false) }
    val confirmPassword = remember { mutableStateOf("") }
    val confirmPasswordVisibility = remember { mutableStateOf(false) }

    val isRestoreButtonEnable: Boolean = remember(password.value, confirmPassword.value) {
        val passwordValue = password.value.trim()
        val confirmPasswordValue = confirmPassword.value.trim()
        val isPasswordValid = passwordValue.isNotEmpty() &&
                passwordValue.matches(Regex(".*(?=.*\\d)(?=.*[a-zA-Z]).*"))
        val isConfirmPasswordValid = confirmPasswordValue.isNotEmpty() &&
                confirmPasswordValue.matches(Regex(".*(?=.*\\d)(?=.*[a-zA-Z]).*"))
        val result = isPasswordValid && isConfirmPasswordValid && passwordValue == confirmPasswordValue
        result
    }

    val lengthValidState: Boolean = remember(password.value) {
        val passwordValue = password.value.trim()
        val value = passwordValue.length >= 8
        Log.d("length_valid", "value: $value")
        value
    }

    val upperCaseValidState: Boolean = remember(password.value) {
        val passwordValue = password.value.trim()
        passwordValue.matches(Regex("(?=.*[A-Z]).*"))
    }

    val lowerCaseValidState: Boolean = remember(password.value) {
        val passwordValue = password.value.trim()
        passwordValue.matches(Regex("(?=.*[a-z]).*"))
    }

    val digitCaseValidState: Boolean = remember(password.value) {
        val passwordValue = password.value.trim()
        passwordValue.matches(Regex("(?=.*\\d).*"))
    }

    Column(
        modifier = Modifier.fillMaxSize()
    ) {
        Toolbar(
            text = "Вход в личный кабинет bcc.kz",
            onBackClick = onBackClick
        )
        Column(
            modifier = Modifier
                .fillMaxSize()
                .absolutePadding(
                    left = 16.dp,
                    right = 16.dp
                )
                .background(Color.White)
                .verticalScroll(rememberScrollState()),
        ) {
            SpacingVertical(80)
            ImageHeader()
            SpacingVertical(50)

            Text(
                text = stringResource(id = R.string.insert_password_title),
                modifier = Modifier
                    .fillMaxWidth()
                    .absolutePadding(
                        left = 20.dp,
                        right = 20.dp,
                        top = 16.dp,
                        bottom = 4.dp
                    )
            )
            PasswordInput(
                passwordState = password,
                passwordVisibility = passwordVisibility
            )
            Text(
                text = stringResource(id = R.string.insert_password_title_again),
                modifier = Modifier
                    .fillMaxWidth()
                    .absolutePadding(
                        left = 20.dp,
                        right = 20.dp,
                        top = 16.dp,
                        bottom = 4.dp
                    )
            )
            PasswordInput(
                passwordState = confirmPassword,
                passwordVisibility = confirmPasswordVisibility
            )

            CheckBoxItem(
                text = "Более 8 символов",
                modifier = Modifier
                    .fillMaxWidth()
                    .absolutePadding(
                        left = 20.dp,
                        right = 20.dp,
                        top = 16.dp,
                        bottom = 4.dp
                    ),
                isChecked = lengthValidState,
            )

            CheckBoxItem(
                text = "Заглавные буквы",
                modifier = Modifier
                    .fillMaxWidth()
                    .absolutePadding(
                        left = 20.dp,
                        right = 20.dp,
                        top = 4.dp,
                        bottom = 4.dp
                    ),
                isChecked = upperCaseValidState,
            )

            CheckBoxItem(
                text = "Прописные буквы",
                modifier = Modifier
                    .fillMaxWidth()
                    .absolutePadding(
                        left = 20.dp,
                        right = 20.dp,
                        top = 4.dp,
                        bottom = 4.dp
                    ),
                isChecked = lowerCaseValidState,
            )

            CheckBoxItem(
                text = "Цифры",
                modifier = Modifier
                    .fillMaxWidth()
                    .absolutePadding(
                        left = 20.dp,
                        right = 20.dp,
                        top = 4.dp,
                        bottom = 4.dp
                    ),
                isChecked = digitCaseValidState,
            )
            SpacingVertical(heightDp = 16)
            MainButton(
                text = stringResource(id = R.string.register),
                enableState = isRestoreButtonEnable,
                onClick = {
                    onRegisterClick.invoke(password.value.trim())
                },
            )
            SpacingVertical(20)
        }
    }
}