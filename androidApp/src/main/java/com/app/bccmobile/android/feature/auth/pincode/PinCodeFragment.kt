package com.app.bccmobile.android.feature.auth.pincode

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.app.bccmobile.android.base.BaseFragment
import com.app.bccmobile.android.databinding.FragmentPinCodeBinding
import com.app.bccmobile.android.di.AppComponent
import com.app.bccmobile.android.utils.showKeyboard
import com.app.bccmobile.presentation.pincode.PinCodeState
import com.app.bccmobile.presentation.pincode.PinCodeViewModel
import com.app.bccmobile.security.keystore.EncryptionFacade
import dev.icerock.moko.mvvm.createViewModelFactory
import dev.icerock.moko.mvvm.dispatcher.eventsDispatcherOnMain
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class PinCodeFragment : BaseFragment<FragmentPinCodeBinding, PinCodeViewModel>(), PinCodeViewModel.EventListener {

    override val viewModelClass: Class<PinCodeViewModel>
        get() = PinCodeViewModel::class.java

    private val pinCodeRouter: PinCodeRouter by lazy{
        PinCodeRouterImpl(fragment = this)
    }

    private val encryptionFacade by lazy {
        EncryptionFacade(context = requireContext())
    }

    private var pinCodeScreenState = PinCodeScreenState.ENTER

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireArguments().getSerializable(PIN_CODE_SCREEN_STATE).let { value ->
            if (value != null) {
                pinCodeScreenState = value as PinCodeScreenState
            }
        }
        Log.d("pin_screen", "state: $pinCodeScreenState")
    }

    override fun viewBindingInflate(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentPinCodeBinding {
        return FragmentPinCodeBinding.inflate(
            inflater, container, false
        ).apply {
            root.setContent {
                PinCodeScreen(
                    pinCodeScreenState = pinCodeScreenState,
                    touchIdState = viewModel.pinCodeState.value,
                    onFingerprintClick = { viewModel.tryToAuth() },
                    onBackClick = { pinCodeRouter.routeBack() },
                    onLogoutClick = { pinCodeRouter.routeToLogin() },
                    onPinCodeValid = { pinCode ->
                        println("is_pin_valid: screen pin: $pinCode")
                        viewModel.comparePinCodes(
                            pinCode = pinCode,
                            decryption = {
                                encryptionFacade.decrypt(pinCode)
                            }
                        )
                    },
                    onPinCodeCreated = { pinCode -> savePinCode(pinCode) }
                )
            }
        }
    }

    override fun viewModelFactory(): ViewModelProvider.Factory {
        return createViewModelFactory {
            AppComponent.factory.pinCodeFactory.createViewModel(
                eventsDispatcher = eventsDispatcherOnMain()
            ).also {
                it.eventsDispatcher.bind(this, this)
                it.biometryAuthenticator.bind(
                    lifecycle = this.lifecycle,
                    fragmentManager = childFragmentManager
                )
                it.biometryAuthenticator.setPackageManager(
                    packageManager = requireContext().packageManager
                )
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showKeyboard()
        observeData()
    }

    override fun onSuccess() {
        Toast.makeText(requireContext(), "Bio success", Toast.LENGTH_SHORT).show()
    }

    override fun onFail() {
        Toast.makeText(requireContext(), "Bio error", Toast.LENGTH_SHORT).show()
    }

    private fun observeData() {
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.pinCodeState.collect { result ->
                when(result) {
                    is PinCodeState.GetPinCode -> {
                        val pin = result.pinCode
                        if (!pin.isNullOrEmpty()) {
                            val decrypted = encryptionFacade.decrypt(pin)
                            Log.d("decrypted_pin", "value: ${decrypted}")
                        }
                    }
                    is PinCodeState.Success -> {
                        Toast.makeText(requireContext(), "SUCCESS", Toast.LENGTH_SHORT).show()
                    }
                    is PinCodeState.Saved -> {

                    }
                }
            }
        }
    }

    private fun savePinCode(pinCode: String) {
        if (pinCodeScreenState == PinCodeScreenState.CREATE) {
            encryptionFacade.createMasterKey(viewModel.biometryAuthenticator, pinCode)
        }
        viewModel.savePinCode(pinCode = encryptionFacade.encrypt(pinCode))
    }

    companion object {

        const val PIN_CODE_SCREEN_STATE = "pin_code_screen_state"

        @JvmStatic
        fun newInstance(pinCodeScreenState: PinCodeScreenState) =
            PinCodeFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(PIN_CODE_SCREEN_STATE, pinCodeScreenState)
                }
            }
    }
}