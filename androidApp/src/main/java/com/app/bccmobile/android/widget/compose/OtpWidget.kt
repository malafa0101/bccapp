package com.app.bccmobile.android.widget.compose

import android.view.LayoutInflater
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import com.app.bccmobile.android.R
import com.app.bccmobile.android.widget.PinEditText

@Composable
fun OtpWidget(
    otpEnterListener: (String) -> Unit,
    otpLength: Int = 6
) {
    AndroidView(
        modifier = Modifier.fillMaxWidth()
            .height(height = 90.dp),
        factory = { context ->
            val view = LayoutInflater.from(context)
                .inflate(R.layout.view_otp, null, false)
            val smsEditText: PinEditText = view.findViewById(R.id.sms_edit_text)
            smsEditText.setOnPinEnteredListener { text ->
                if (text.length >= otpLength) {
                    otpEnterListener.invoke(text.toString())
                    smsEditText.text?.clear()
                }
            }
            view
        }
    )
}