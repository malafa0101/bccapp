package com.app.bccmobile.android.feature.auth.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.app.bccmobile.android.base.BaseFragment
import com.app.bccmobile.android.databinding.FragmentLoginBinding
import com.app.bccmobile.android.di.AppComponent
import com.app.bccmobile.android.feature.auth.login.router.LoginRouter
import com.app.bccmobile.android.feature.auth.login.router.LoginRouterImpl
import com.app.bccmobile.android.utils.showToast
import com.app.bccmobile.presentation.login.LoginState
import com.app.bccmobile.presentation.login.LoginViewModel
import com.app.bccmobile.presentation.state.LoadingState
import dev.icerock.moko.mvvm.createViewModelFactory
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class LoginFragment : BaseFragment<FragmentLoginBinding, LoginViewModel>() {

    override val viewModelClass: Class<LoginViewModel>
        get() = LoginViewModel::class.java

    private val loginRouter: LoginRouter by lazy{
        LoginRouterImpl(fragment = this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun viewBindingInflate(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentLoginBinding {
        return FragmentLoginBinding.inflate(
            inflater, container, false
        ).apply {
            this.root.setContent {
                LoginScreen(
                    onEnterClick = { phone, password ->
                        viewModel.login(phone = phone, password = password)
                    },
                    onForgotPasswordClick = { loginRouter.routeToForgotPassword() },
                    onBackClick = { loginRouter.routeBack() },
                    onPinCodeClick = { loginRouter.routeToPinCode() }
                )
            }
        }
    }

    override fun viewModelFactory(): ViewModelProvider.Factory  = createViewModelFactory {
        AppComponent.factory.loginFactory.createViewModel()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeData()
        observeLoading()
        observeError()
    }

    private fun observeData() = viewLifecycleOwner.lifecycleScope.launch {
        viewModel.loginState.collect { result ->
            when (result) {
                is LoginState.Success -> {
                    loginRouter.routeToMenu()
                }
            }
        }
    }

    private fun observeLoading() = viewLifecycleOwner.lifecycleScope.launch {
        viewModel.loadingState.collect { result ->
            when(result) {
                LoadingState.Show -> showLoading()
                LoadingState.Hide -> hideLoading()
            }
        }
    }

    private fun observeError() = viewLifecycleOwner.lifecycleScope.launch {
        viewModel.errorState.collect { error ->
            showToast(error)
        }
    }
}