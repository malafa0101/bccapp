package com.app.bccmobile.android.base

import com.app.bccmobile.android.widget.CircleProgress
import dev.icerock.moko.mvvm.viewbinding.MvvmFragment

abstract class BaseFragment<VB: androidx.viewbinding.ViewBinding, VM: dev.icerock.moko.mvvm.viewmodel.ViewModel> : MvvmFragment<VB, VM>() {

    protected val progressView: CircleProgress by lazy { CircleProgress(requireActivity()) }

    override fun onDestroyView() {
        progressView.dismiss()
        super.onDestroyView()
    }

    fun showLoading() {
        progressView.show()
    }

    fun hideLoading() {
        progressView.dismiss()
    }
}