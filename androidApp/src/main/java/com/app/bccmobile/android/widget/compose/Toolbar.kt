package com.app.bccmobile.android.widget.compose

import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.app.bccmobile.android.R

@Composable
fun Toolbar(
    text: String,
    onBackClick: () -> Unit
) {
    TopAppBar(
        title = {
            Text(
                text = text,
                maxLines = 1,
            )
        },
        navigationIcon = {
            IconButton(
                onClick = onBackClick
            ) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_arrow_back),
                    contentDescription = ""
                )
            }
        },
        backgroundColor = Color.White,
        elevation = 0.dp
    )
}