package com.app.bccmobile.android.feature.menu

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.navigation.NavController
import com.app.bccmobile.android.R
import com.app.bccmobile.android.utils.hideKeyboard
import com.app.bccmobile.android.utils.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView

class MenuFragment : Fragment(R.layout.fragment_menu) {

    private lateinit var bnv: BottomNavigationView
    private var currentNavController: LiveData<NavController>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindViews(view)
        hideKeyboard()
    }

    private fun bindViews(view: View) {
        bnv = view.findViewById(R.id.bnv)
        val navGraphIds = listOf(
            R.navigation.nav_home,
            R.navigation.nav_search,
            R.navigation.nav_give,
            R.navigation.nav_notification,
            R.navigation.nav_profile
        )
        currentNavController = bnv.setupWithNavController(
            navGraphIds,
            childFragmentManager,
            R.id.nav_host_fragment,
            requireActivity().intent
        )
    }
}