package com.app.bccmobile.android.widget

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.view.View
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.core.content.res.ResourcesCompat
import com.app.bccmobile.android.BuildConfig
import com.app.bccmobile.android.R
import com.app.bccmobile.android.utils.toDp

class CircleProgress(activity: Activity) {

    private val dialog: Dialog

    init {
        val progressBar = ProgressBar(activity, null, android.R.attr.progressBarStyle)
        progressBar.isIndeterminate = true
        progressBar.visibility = View.VISIBLE
        val layout = RelativeLayout(activity)
        val params = RelativeLayout.LayoutParams(
            RelativeLayout.LayoutParams.MATCH_PARENT,
            RelativeLayout.LayoutParams.MATCH_PARENT
        )
        params.addRule(RelativeLayout.CENTER_IN_PARENT)
        val margin = 8.toDp()
        params.setMargins(margin, margin, margin, margin)
        layout.addView(progressBar, params)
        dialog = AlertDialog.Builder(activity)
            .setView(layout)
            .setCancelable(BuildConfig.DEBUG)
            .create()
        val drawable = ResourcesCompat.getDrawable(
            activity.resources,
            R.drawable.rounded_frame_layout,
            null
        )
        val color = ResourcesCompat.getColor(activity.resources, R.color.green27AE60, null)
        progressBar.indeterminateDrawable.setColorFilter(color, android.graphics.PorterDuff.Mode.MULTIPLY)
        dialog.window?.setBackgroundDrawable(drawable)
    }


    fun show(size: Int = 60) {
        dialog.show()
        dialog.window?.setLayout(size.toDp(), size.toDp())
    }

    fun dismiss() {
        dialog.dismiss()
    }
}
