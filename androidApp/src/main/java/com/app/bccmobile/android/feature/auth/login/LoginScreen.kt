package com.app.bccmobile.android.feature.auth.login

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.absolutePadding
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.app.bccmobile.android.R
import com.app.bccmobile.android.utils.formattedPhoneIsValid
import com.app.bccmobile.android.widget.compose.*

// todo translate
@Composable
fun LoginScreen(
    onEnterClick: (String, String) -> Unit,
    onForgotPasswordClick: () -> Unit,
    onBackClick: () -> Unit,
    onPinCodeClick: () -> Unit
) {
    val phone = remember { mutableStateOf("") }
    val password = remember { mutableStateOf("") }
    val passwordVisibility = remember { mutableStateOf(false) }
    val isLoginButtonEnable: Boolean = remember(phone.value, password.value) {
        val phoneIsValid = phone.value.trim().isNotEmpty() && phone.value.trim().formattedPhoneIsValid()
        val passwordIsNotEmpty = password.value.trim().isNotEmpty()
        phoneIsValid && passwordIsNotEmpty
    }

    val checkBoxState = remember { mutableStateOf(false) }

    Column(
        modifier = Modifier.fillMaxSize()
    ) {
        Toolbar(
            text =  stringResource(id = R.string.login_toolbar_title),
            onBackClick = onBackClick
        )
        Column(
            modifier = Modifier
                .fillMaxSize()
                .absolutePadding(
                    left = 16.dp,
                    right = 16.dp
                )
                .background(Color.White)
                .verticalScroll(rememberScrollState()),
        ) {
            SpacingVertical(80)
            ImageHeader()
            Text(
                text = "Введите ваш номер",
                modifier = Modifier
                    .fillMaxWidth()
                    .absolutePadding(
                        left = 20.dp,
                        right = 20.dp,
                        top = 24.dp,
                        bottom = 4.dp
                    ),
                style = MaterialTheme.typography.caption
            )
            PhoneNumberInput(numberState = phone)
            Text(
                text = "Введите пароль",
                modifier = Modifier
                    .fillMaxWidth()
                    .absolutePadding(
                        left = 20.dp,
                        right = 20.dp,
                        top = 16.dp,
                        bottom = 4.dp
                    ),
                style = MaterialTheme.typography.caption
            )
            PasswordInput(
                passwordState = password,
                passwordVisibility = passwordVisibility
            )
            CheckBoxItem(
                text = "Запомнить",
                modifier = Modifier
                    .fillMaxWidth()
                    .absolutePadding(
                        left = 20.dp,
                        right = 20.dp,
                        top = 4.dp,
                        bottom = 4.dp
                    ),
                formState = checkBoxState,
                isClickable = true
            )
            Column(
                modifier = Modifier.fillMaxWidth(),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(
                    text = "Забыли пароль?",
                    modifier = Modifier
                        .noRippleClickable(onClick = onForgotPasswordClick)
                        .absolutePadding(top = 30.dp),
                    color = colorResource(id = R.color.green27AE60),
                    fontSize = 16.sp
                )
            }
            SpacingVertical(80)
            MainButton(
                text = "Войти",
                enableState = isLoginButtonEnable,
                onClick = {
                    onEnterClick.invoke(
                        phone.value,
                        password.value
                    )
                },
            )
            SpacingVertical(heightDp = 60)
            ButtonFingerprint(onClick = onPinCodeClick)
            SpacingVertical(52)
        }
    }
}