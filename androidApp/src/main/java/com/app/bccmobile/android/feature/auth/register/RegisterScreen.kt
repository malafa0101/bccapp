package com.app.bccmobile.android.feature.auth.register

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.absolutePadding
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.app.bccmobile.android.R
import com.app.bccmobile.android.utils.formattedPhoneIsValid
import com.app.bccmobile.android.widget.compose.*

// todo translate
@Composable
fun RegisterScreen(
    onRestorePasswordClick: (String) -> Unit,
    onRegisterClick: () -> Unit,
    onBackClick: () -> Unit
) {
    val phone = remember { mutableStateOf("") }
    val isButtonEnable: Boolean = remember(phone.value) {
        val phoneValue = phone.value.trim()
        val phoneIsValid = phoneValue.isNotEmpty() && phoneValue.formattedPhoneIsValid()
        phoneIsValid
    }

    Column(
        modifier = Modifier.fillMaxSize()
    ) {
        Toolbar(
            text = stringResource(id = R.string.login_toolbar_title),
            onBackClick = onBackClick
        )
        Column(
            modifier = Modifier
                .fillMaxSize()
                .absolutePadding(
                    left = 16.dp,
                    right = 16.dp
                )
                .background(Color.White)
                .verticalScroll(rememberScrollState()),
        ) {
            SpacingVertical(80)
            ImageHeader()
            NoAccRegisterWidget(onRegisterClick)
            Text(
                text = "Введите ваш номер",
                modifier = Modifier
                    .fillMaxWidth()
                    .absolutePadding(
                        left = 32.dp,
                        right = 20.dp,
                        top = 24.dp,
                        bottom = 4.dp
                    ),
                style = MaterialTheme.typography.caption
            )
            PhoneNumberInput(numberState = phone)

            MainButton(
                text = stringResource(id = R.string.continue_title),
                enableState = isButtonEnable,
                onClick = {
                    onRestorePasswordClick.invoke(phone.value)
                },
            )
            SpacingVertical(20)
            Text(
                text = "или",
                color = colorResource(id = R.color.grayA9ACB0),
                modifier = Modifier
                    .fillMaxWidth(),
                textAlign = TextAlign.Center,
                style = MaterialTheme.typography.button
            )
            SpacingVertical(20)
            ButtonIcon(
                text = stringResource(id = R.string.google),
                onClick = {
                    onRestorePasswordClick.invoke(phone.value)
                },
                icon = R.drawable.ic_google
            )
            SpacingVertical(20)
            ButtonIcon(
                text = stringResource(id = R.string.facebook),
                onClick = {
                    onRestorePasswordClick.invoke(phone.value)
                },
                icon = R.drawable.ic_facebook
            )
            SpacingVertical(20)
            ButtonIcon(
                text = stringResource(id = R.string.vk),
                onClick = {
                    onRestorePasswordClick.invoke(phone.value)
                },
                icon = R.drawable.ic_vk
            )
            SpacingVertical(heightDp = 60)
            ButtonFingerprint {

            }
            SpacingVertical(20)
        }
    }
}