package com.app.bccmobile.android.widget.compose

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.app.bccmobile.android.R

@Composable
fun ButtonFingerprint(
    onClick: () -> Unit
) {
    Column(
        modifier = Modifier.fillMaxWidth(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Image(
            modifier = Modifier
                .height(height = 50.dp)
                .width(width = 45.dp)
                .noRippleClickable(onClick = onClick),
            painter = painterResource(id = R.drawable.ic_fingerprint),
            contentDescription = "",
            alignment = Alignment.Center,
            contentScale = ContentScale.Crop
        )
    }
}