package com.app.bccmobile.android.widget.compose

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.OutlinedButton
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.app.bccmobile.android.R

@Composable
fun PinButtonsGroup(
    pinInputState: MutableState<String>,
    touchIdEnable: Boolean,
    onFingerprintClick: () -> Unit
) {
    val context = LocalContext.current

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(start = 60.dp, end = 60.dp)
    ) {
        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier.fillMaxWidth()
        ) {
            PinNumberButton(text = "1") {
                pinInputState.value = pinInputState.value.plus("1")
            }
            PinNumberButton(text = "2") {
                pinInputState.value = pinInputState.value.plus("2")
            }
            PinNumberButton(text = "3") {
                pinInputState.value = pinInputState.value.plus("3")
            }
        }
        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 0.dp)
        ) {
            PinNumberButton(text = "4") {
                pinInputState.value = pinInputState.value.plus("4")
            }
            PinNumberButton(text = "5") {
                pinInputState.value = pinInputState.value.plus("5")
            }
            PinNumberButton(text = "6") {
                pinInputState.value = pinInputState.value.plus("6")
            }
        }
        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 0.dp)
        ) {
            PinNumberButton(text = "7") {
                pinInputState.value = pinInputState.value.plus("7")
            }
            PinNumberButton(text = "8") {
                pinInputState.value = pinInputState.value.plus("8")
            }
            PinNumberButton(text = "9") {
                pinInputState.value = pinInputState.value.plus("9")
            }
        }
        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 0.dp)
        ) {
            if (touchIdEnable) {
                PinImageButton(
                    onClick = onFingerprintClick,
                    backgroundColor = colorResource(id = R.color.green27AE60),
                ) {
                    Image(
                        modifier = Modifier.size(32.dp),
                        painter = painterResource(id = R.drawable.ic_fingerprint_white),
                        contentDescription = ""
                    )
                }
            } else {
                Box(
                    modifier = Modifier
                        .padding(all = 12.dp)
                        .size(56.dp)
                )
            }
            PinNumberButton(text = "0") {
                pinInputState.value = pinInputState.value.plus("0")
            }
            PinImageButton(
                onClick = {
                    pinInputState.value = pinInputState.value.dropLast(1)
                },
                backgroundColor = colorResource(id = R.color.white),
            ) {
                Image(
                    modifier = Modifier.size(32.dp),
                    painter = painterResource(id = R.drawable.ic_close),
                    contentDescription = ""
                )
            }
        }
    }
}

@Composable
internal fun PinNumberButton(
    text: String,
    onClick: () -> Unit,
) {
    OutlinedButton(
        onClick = onClick,
        modifier = Modifier
            .padding(all = 12.dp)
            .size(56.dp),
        shape = CircleShape,
        border = null,
        contentPadding = PaddingValues(0.dp),
        colors = ButtonDefaults.outlinedButtonColors(
            contentColor =  colorResource(id = R.color.gray3A3A3F),
            backgroundColor = colorResource(id = R.color.grayE9E9E9),
            disabledContentColor = colorResource(id = R.color.grayE9E9E9)
        )
    ) {
        Text(
            text = text,
            textAlign = TextAlign.Center,
            fontSize = 24.sp
        )
    }
}


@Composable
internal fun PinImageButton(
    onClick: () -> Unit,
    backgroundColor: Color,
    content: @Composable RowScope.() -> Unit,
) {
    OutlinedButton(
        onClick = onClick,
        modifier = Modifier
            .padding(all = 12.dp)
            .size(56.dp),
        shape = CircleShape,
        border = null,
        contentPadding = PaddingValues(0.dp),
        colors = ButtonDefaults.outlinedButtonColors(
            backgroundColor = backgroundColor,
        ),
        content = content
    )
}