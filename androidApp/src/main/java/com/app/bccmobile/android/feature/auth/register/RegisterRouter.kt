package com.app.bccmobile.android.feature.auth.register

import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.app.bccmobile.android.R
import com.app.bccmobile.android.feature.auth.otp.OtpFragment

interface RegisterRouter {

    fun routeToLogin()

    fun routeToOtp(phone: String)

    fun routeBack()
}

class RegisterRouterImpl(
    private val fragment: Fragment
) : RegisterRouter {

    override fun routeToLogin() {
        fragment.findNavController()
            .navigate(R.id.action_registerFragment_to_loginFragment)
    }

    override fun routeToOtp(phone: String) {
        fragment.findNavController()
            .navigate(
                R.id.action_registerFragment_to_otpFragment,
                bundleOf(OtpFragment.PHONE to phone)
            )
    }

    override fun routeBack() {
        fragment.findNavController().navigateUp()
    }

}