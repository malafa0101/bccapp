package com.app.bccmobile.android.feature.auth.forgot_password

import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.app.bccmobile.android.R
import com.app.bccmobile.android.feature.auth.otp.OtpFragment

interface ForgotPasswordRouter {

    fun routeBack()

    fun routeToOtpScreen(phone: String)

    fun routeRegister()
}

class ForgotPasswordRouterImpl(
    private val fragment: Fragment
) : ForgotPasswordRouter {

    override fun routeBack() {
        fragment.findNavController().navigateUp()
    }

    override fun routeToOtpScreen(phone: String) {
        fragment.findNavController()
            .navigate(
                R.id.action_forgotPasswordFragment_to_otpFragment,
                bundleOf(
                    OtpFragment.PHONE to phone,
                    OtpFragment.RESET_PASSWORD to true
                )
            )
    }

    override fun routeRegister() {
        fragment.findNavController()
            .navigate(R.id.action_forgotPasswordFragment_to_registerFragment)
    }

}