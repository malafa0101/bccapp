package com.app.bccmobile.android.widget.compose

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.absolutePadding
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp

@Composable
fun ButtonIcon(
    text: String,
    onClick: () -> Unit,
    icon: Int
) {
    OutlinedButton(
        onClick = onClick,
        modifier = Modifier
            .fillMaxWidth()
            .height(height = 48.dp)
            .absolutePadding(
                left = 10.dp,
                right = 10.dp
            ),
        enabled = true,
        border = BorderStroke(1.dp, color = Color(0xFFA9ACB0)),
        colors = ButtonDefaults.buttonColors(
            backgroundColor = Color.White,
            contentColor = Color(0xFFA9ACB0)
        ),
        shape = RoundedCornerShape(12.dp),
    ) {
        Icon(
            painter = painterResource(id = icon),
            contentDescription = null,
            modifier = Modifier.padding(end = 19.52.dp),
            tint = Color(0xFF3A3A3F),
        )
        Text(
            text = text, style = MaterialTheme.typography.button
        )
    }
}