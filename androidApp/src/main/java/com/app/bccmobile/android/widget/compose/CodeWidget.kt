package com.app.bccmobile.android.widget.compose

import androidx.compose.foundation.layout.*
import androidx.compose.material.Divider
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp

@Composable
fun CodeWidget(
    v1: String,
    v2: String,
    v3: String,
    v4: String,
    v5: String,
    v6: String
) {
    Row(
        modifier = Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.Center
    ) {
        Column(
            modifier = Modifier.width(32.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = v1,
                style = MaterialTheme.typography.h1,
                textAlign = TextAlign.Center
            )
            Divider(color = Color(0xFFB10909), thickness = 1.dp)
        }
        SpacingHorizontal(widthDp = 8)
        Column(
            modifier = Modifier.width(32.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = v2,
                style = MaterialTheme.typography.h1,
                textAlign = TextAlign.Center
            )
            Divider(color = Color(0xFFB10909), thickness = 1.dp)
        }
        SpacingHorizontal(widthDp = 8)
        Column(
            modifier = Modifier.width(32.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = v3,
                style = MaterialTheme.typography.h1,
                textAlign = TextAlign.Center
            )
            Divider(color = Color(0xFFB10909), thickness = 1.dp)
        }
        SpacingHorizontal(widthDp = 8)
        Column(
            modifier = Modifier.width(32.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = v4,
                style = MaterialTheme.typography.h1,
                textAlign = TextAlign.Center
            )
            Divider(color = Color(0xFFB10909), thickness = 1.dp)
        }
        SpacingHorizontal(widthDp = 8)
        Column(
            modifier = Modifier.width(32.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = v5,
                style = MaterialTheme.typography.h1,
                textAlign = TextAlign.Center
            )
            Divider(color = Color(0xFFB10909), thickness = 1.dp)
        }
        SpacingHorizontal(widthDp = 8)
        Column(
            modifier = Modifier.width(32.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = v6,
                style = MaterialTheme.typography.h1,
                textAlign = TextAlign.Center
            )
            Divider(color = Color(0xFFB10909), thickness = 1.dp)
        }
    }
}