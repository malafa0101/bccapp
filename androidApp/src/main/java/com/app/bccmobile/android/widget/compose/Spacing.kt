package com.app.bccmobile.android.widget.compose

import androidx.compose.foundation.layout.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@Composable
fun SpacingVertical(heightDp: Int) {
    Spacer(
        modifier = Modifier
            .height(heightDp.dp)
            .fillMaxWidth()
    )
}
@Composable
fun SpacingHorizontal(widthDp: Int) {
    Spacer(
        modifier = Modifier
            .width(widthDp.dp)
            .fillMaxHeight()
    )
}