package com.app.bccmobile.android.feature.auth.otp

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.app.bccmobile.android.R
import com.app.bccmobile.android.utils.PhoneMask
import com.app.bccmobile.android.utils.formatPhone
import com.app.bccmobile.android.widget.compose.OtpWidget
import com.app.bccmobile.android.widget.compose.SpacingVertical
import com.app.bccmobile.android.widget.compose.ToolbarCross
import com.app.bccmobile.android.widget.compose.noRippleClickable

@Composable
fun OTPScreen(
    phone: String,
    onBackClick: () -> Unit,
    onResendClick: () -> Unit,
    otpEnterListener: (String) -> Unit
) {
    Column(
        modifier = Modifier.fillMaxSize()
    ) {
        SpacingVertical(54)
        ToolbarCross(
            text = stringResource(id = R.string.sms_toolbar_title),
            onBackClick = onBackClick
        )
        SpacingVertical(80)
        Column(
            modifier = Modifier
                .fillMaxSize()
                .absolutePadding(
                    left = 16.dp,
                    right = 16.dp
                )
                .background(Color.White)
                .verticalScroll(rememberScrollState()),
        ) {
            Text(
                text = String.format(
                    stringResource(id = R.string.sms_info),
                    phone.formatPhone(mask = PhoneMask.MASK_2)
                ),
                color = colorResource(id = R.color.grayA9ACB0),
                modifier = Modifier
                    .fillMaxWidth()
                    .absolutePadding(left = 24.dp, right = 24.dp),
                textAlign = TextAlign.Center,
                fontSize = 18.sp
            )
            SpacingVertical(20)
            Box(
                modifier = Modifier.fillMaxWidth()
                    .absolutePadding(left = 48.dp, right = 48.dp)
            ) {
                OtpWidget(otpEnterListener = otpEnterListener)
            }
            SpacingVertical(60)
            Column(
                modifier = Modifier.fillMaxWidth(),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(
                    text = stringResource(id = R.string.send_again_toolbar_title),
                    modifier = Modifier
                        .absolutePadding(left = 16.dp)
                        .noRippleClickable(onClick = onResendClick),
                    color = colorResource(id = R.color.green27AE60),
                    fontSize = 15.sp,
                    textAlign = TextAlign.Center,
                    style = MaterialTheme.typography.button
                )
            }
        }
    }
}