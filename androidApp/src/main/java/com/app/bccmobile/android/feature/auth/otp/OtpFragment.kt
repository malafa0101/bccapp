package com.app.bccmobile.android.feature.auth.otp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.app.bccmobile.android.base.BaseFragment
import com.app.bccmobile.android.databinding.FragmentOtpBinding
import com.app.bccmobile.android.di.AppComponent
import com.app.bccmobile.android.utils.formatPhone
import com.app.bccmobile.android.utils.showToast
import com.app.bccmobile.presentation.otp.OtpState
import com.app.bccmobile.presentation.otp.OtpViewModel
import com.app.bccmobile.presentation.state.LoadingState
import dev.icerock.moko.mvvm.createViewModelFactory
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class OtpFragment : BaseFragment<FragmentOtpBinding, OtpViewModel>() {

    override val viewModelClass: Class<OtpViewModel>
        get() = OtpViewModel::class.java

    private val otpRouter: OtpRouter by lazy {
        OtpRouterImpl(fragment = this)
    }

    private var phone: String? = null
    private var resetPassword: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        phone = requireArguments().getString(PHONE)
        resetPassword = requireArguments().getBoolean(RESET_PASSWORD)
        viewModel.sendOpt(phone = requireNotNull(phone).formatPhone())
    }

    override fun viewBindingInflate(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentOtpBinding {
        return FragmentOtpBinding.inflate(
            inflater,
            container,
            false
        ).apply {
            root.setContent {
                OTPScreen(
                    phone = requireNotNull(phone),
                    onBackClick = {
                        otpRouter.exit()
                    },
                    onResendClick = {
                        viewModel.sendOpt(phone = requireNotNull(phone))
                    },
                    otpEnterListener = { otpCode ->
                        otpRouter.routeToRegisterFinish(
                            otp = otpCode,
                            phone = requireNotNull(phone),
                            resetPassword = resetPassword
                        )
                    }
                )
            }
        }
    }

    override fun viewModelFactory(): ViewModelProvider.Factory {
        return createViewModelFactory {
            AppComponent.factory.otpFactory.createViewModel()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeLoading()
        observeOtpResult()
        observeError()
    }

    private fun observeOtpResult() {
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.otpState.collect { result ->
                when(result) {
                    is OtpState.Success -> {

                    }
                }
            }
        }
    }

    private fun observeLoading() {
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.loadingState.collect { result ->
                when(result) {
                    is LoadingState.Show -> showLoading()
                    is LoadingState.Hide -> hideLoading()
                }
            }
        }
    }

    private fun observeError() = viewLifecycleOwner.lifecycleScope.launch {
        viewModel.errorState.collect { error ->
            showToast(error)
        }
    }

    companion object {
        const val PHONE = "phone"
        const val RESET_PASSWORD = "reset_password"

        @JvmStatic
        fun newInstance() =
            OtpFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }
}