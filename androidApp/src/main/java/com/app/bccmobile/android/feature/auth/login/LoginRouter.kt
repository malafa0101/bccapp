package com.app.bccmobile.android.feature.auth.login.router

import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.app.bccmobile.android.R
import com.app.bccmobile.android.feature.auth.pincode.PinCodeFragment
import com.app.bccmobile.android.feature.auth.pincode.PinCodeScreenState

interface LoginRouter {

    fun routeToOtpScreen(phone: String)

    fun routeToRegisterScreen()

    fun routeToMenu()

    fun routeBack()

    fun routeToForgotPassword()

    fun routeToPinCode()
}

class LoginRouterImpl(
    private val fragment: Fragment
) : LoginRouter {

    override fun routeToOtpScreen(phone: String) {
//        fragment.findNavController()
//            .navigate(
//                R.id.action_loginFragment_to_otpFragment,
//                bundleOf("phone" to phone)
//            )
    }

    override fun routeToRegisterScreen() {
        fragment.findNavController()
    }

    override fun routeToMenu() {
        fragment.findNavController()
            .navigate(
                R.id.action_loginFragment_to_nav_menu
            )
    }

    override fun routeBack() {
        fragment.findNavController().navigateUp()
    }

    override fun routeToForgotPassword() {
        fragment.findNavController().navigate(R.id.action_loginFragment_to_forgotPasswordFragment)
    }

    override fun routeToPinCode() {
        fragment.findNavController().navigate(
            R.id.action_loginFragment_to_pinCodeFragment,
            bundleOf(
                PinCodeFragment.PIN_CODE_SCREEN_STATE to PinCodeScreenState.ENTER
            )
        )
    }

}