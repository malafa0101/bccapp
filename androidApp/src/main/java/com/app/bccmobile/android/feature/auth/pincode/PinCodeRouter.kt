package com.app.bccmobile.android.feature.auth.pincode

import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.app.bccmobile.android.R

interface PinCodeRouter {

    fun routeToLogin()

    fun routeBack()
}

class PinCodeRouterImpl(
    private val fragment: Fragment
) : PinCodeRouter {

    override fun routeToLogin() {
        fragment.findNavController()
            .navigate(
                R.id.action_pinCodeFragment_to_loginFragment
            )
    }

    override fun routeBack() {
        fragment.findNavController().navigateUp()
    }

}