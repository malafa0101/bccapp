package com.app.bccmobile.android.feature.auth.forgot_password

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.app.bccmobile.android.databinding.FragmentForgotPasswordBinding
import com.app.bccmobile.android.di.AppComponent
import com.app.bccmobile.presentation.registration.RegisterViewModel
import dev.icerock.moko.mvvm.createViewModelFactory
import dev.icerock.moko.mvvm.viewbinding.MvvmFragment
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class ForgotPasswordFragment : MvvmFragment<FragmentForgotPasswordBinding, RegisterViewModel>() {

    override val viewModelClass: Class<RegisterViewModel>
        get() = RegisterViewModel::class.java

    private val forgotPasswordRouter by lazy {
        ForgotPasswordRouterImpl(fragment = this)
    }

    override fun viewBindingInflate(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentForgotPasswordBinding {
        return FragmentForgotPasswordBinding.inflate(
            inflater,
            container,
            false
        ).apply {
            this.root.setContent {
                ForgotPasswordScreen(
                    onRestorePasswordClick = { phone ->
                        forgotPasswordRouter.routeToOtpScreen(phone = phone)
                    },
                    onRegisterClick = { forgotPasswordRouter.routeRegister() },
                    onBackClick = { forgotPasswordRouter.routeBack() }
                )
            }
        }
    }

    override fun viewModelFactory(): ViewModelProvider.Factory {
        return createViewModelFactory {
            AppComponent.factory.registerFactory.createViewModel()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeData()
    }

    private fun observeData() {
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.loadingState.collect { result ->

            }
        }
    }
}