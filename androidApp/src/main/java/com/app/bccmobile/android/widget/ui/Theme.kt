package com.app.bccmobile.android.widget.ui

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Typography
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp

private val DarkColorPalette = darkColors(
    primary = Purple200,
    primaryVariant = Purple700,
    secondary = Teal200
)

private val LightColorPalette = lightColors(
    primary = Purple500,
    primaryVariant = Purple700,
    secondary = Teal200

    /* Other default colors to override
    background = Color.White,
    surface = Color.White,
    onPrimary = Color.White,
    onSecondary = Color.Black,
    onBackground = Color.Black,
    onSurface = Color.Black,
    */
)

val MyTypography = Typography(
    subtitle2 = TextStyle(
        fontFamily = sf_pro_display_family,
        fontSize = 10.sp,
        letterSpacing = 0.sp
    ),
    subtitle1 = TextStyle(
        fontFamily = sf_pro_display_family,
        fontSize = 12.sp,
        letterSpacing = 0.sp
    ),
    caption = TextStyle(
        fontFamily = sf_pro_display_family,
        fontSize = 14.sp,
        letterSpacing = 0.sp
    ),
    body2 = TextStyle(
        fontFamily = sf_pro_display_family,
        fontSize = 16.sp,
        letterSpacing = 0.sp
    ),
    body1 = TextStyle(
        fontFamily = sf_pro_display_family,
        fontSize = 18.sp,
        letterSpacing = 0.sp
    ),
    h4 = TextStyle(
        fontFamily = sf_pro_display_family,
        fontSize = 18.sp,
        letterSpacing = 0.sp,
        fontWeight = FontWeight.Medium
    ),
    button = TextStyle(
        fontFamily = sf_pro_display_family,
        fontSize = 16.sp,
        letterSpacing = 0.sp,
        fontWeight = FontWeight.Medium
    ),
    h3 = TextStyle(
        fontFamily = sf_pro_display_family,
        fontSize = 24.sp,
        letterSpacing = 0.sp
    ),
    h1 = TextStyle(
        fontFamily = sf_pro_display_family,
        fontSize = 28.sp,
        letterSpacing = 0.sp
    ),
    h2 = TextStyle(
        fontFamily = axiforma_family,
        fontSize = 24.sp,
        letterSpacing = 0.sp
    )
)

@Composable
fun BccComposeTheme(
    typography: Typography = MyTypography,
    darkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable() () -> Unit
) {
    val colors = if (darkTheme) {
        DarkColorPalette
    } else {
        LightColorPalette
    }

    MaterialTheme(
        colors = colors,
        typography = MyTypography,
        shapes = Shapes,
        content = content
    )
}