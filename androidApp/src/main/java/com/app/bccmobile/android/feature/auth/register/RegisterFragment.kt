package com.app.bccmobile.android.feature.auth.register

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.app.bccmobile.android.base.BaseFragment
import com.app.bccmobile.android.databinding.FragmentRegisterBinding
import com.app.bccmobile.android.di.AppComponent
import com.app.bccmobile.android.utils.formatPhone
import com.app.bccmobile.presentation.registration.RegisterState
import com.app.bccmobile.presentation.registration.RegisterViewModel
import com.app.bccmobile.presentation.state.LoadingState
import dev.icerock.moko.mvvm.createViewModelFactory
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class RegisterFragment : BaseFragment<FragmentRegisterBinding, RegisterViewModel>() {

    override val viewModelClass: Class<RegisterViewModel>
        get() = RegisterViewModel::class.java

    private val registerRouter by lazy {
        RegisterRouterImpl(fragment = this)
    }

    override fun viewBindingInflate(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentRegisterBinding {
        return FragmentRegisterBinding.inflate(
            inflater,
            container,
            false
        ).apply {
            this.root.setContent {
                RegisterScreen(
                    onRestorePasswordClick = { phone ->
                        viewModel.checkUser(phone = phone.formatPhone())
                    },
                    onRegisterClick = {
                        registerRouter.routeToLogin()
                    },
                    onBackClick = { registerRouter.routeBack() }
                )
            }
        }
    }

    override fun viewModelFactory(): ViewModelProvider.Factory {
        return createViewModelFactory {
            AppComponent.factory.registerFactory.createViewModel()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeData()
    }

    private fun observeData() {
        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            viewModel.registrationState.collect { result ->
                when (result) {
                    is RegisterState.Login -> {
                        registerRouter.routeToLogin()
                    }
                    is RegisterState.Register -> {
                        registerRouter.routeToOtp(phone = result.phone)
                    }
                }
            }
        }
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.loadingState.collect { result ->
                when (result) {
                    LoadingState.Show -> showLoading()
                    LoadingState.Hide -> hideLoading()
                }
            }
        }
    }

    companion object {
        fun newInstance() =
            RegisterFragment().apply {

            }
    }
}