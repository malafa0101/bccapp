package com.app.bccmobile.android.feature.menu.profile

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.app.bccmobile.android.base.BaseFragment
import com.app.bccmobile.android.databinding.FragmentProfileBinding
import com.app.bccmobile.android.di.AppComponent
import com.app.bccmobile.android.utils.showToast
import com.app.bccmobile.presentation.profile.ProfileViewModel
import com.app.bccmobile.presentation.state.LoadingState
import dev.icerock.moko.mvvm.createViewModelFactory
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class ProfileFragment : BaseFragment<FragmentProfileBinding, ProfileViewModel>() {

    override val viewModelClass: Class<ProfileViewModel>
        get() = ProfileViewModel::class.java

    override fun viewBindingInflate(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentProfileBinding {
        return FragmentProfileBinding.inflate(inflater, container, false)
    }

    override fun viewModelFactory(): ViewModelProvider.Factory {
        return createViewModelFactory {
            AppComponent.factory.profileFactory.createViewModel()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.getUser()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeData()
        observeError()
        observeLoading()
    }

    private fun observeData() = viewLifecycleOwner.lifecycleScope.launch {
        viewModel.profileState.collect { result ->
            Log.d("profile_data", result?.toString() ?: "null")
        }
    }

    private fun observeLoading() = viewLifecycleOwner.lifecycleScope.launch {
        viewModel.loadingState.collect { result ->
            when (result) {
               is LoadingState.Show -> showLoading()
               is LoadingState.Hide -> hideLoading()
            }
        }
    }

    private fun observeError() = viewLifecycleOwner.lifecycleScope.launch {
        viewModel.errorState.collect { error ->
            showToast(error)
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            ProfileFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }

}