package com.app.bccmobile.android.feature.auth.register_finish

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.app.bccmobile.android.base.BaseFragment
import com.app.bccmobile.android.databinding.FragmentRegistrationFinishBinding
import com.app.bccmobile.android.di.AppComponent
import com.app.bccmobile.android.utils.formatPhone
import com.app.bccmobile.android.utils.showToast
import com.app.bccmobile.presentation.registration_finish.RegisterFinishState
import com.app.bccmobile.presentation.registration_finish.RegistrationFinishViewModel
import com.app.bccmobile.presentation.state.LoadingState
import dev.icerock.moko.mvvm.createViewModelFactory
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class RegisterFinishFragment : BaseFragment<FragmentRegistrationFinishBinding, RegistrationFinishViewModel>() {

    override val viewModelClass: Class<RegistrationFinishViewModel>
        get() = RegistrationFinishViewModel::class.java

    private val registerFinishRouter by lazy {
        RegisterFinishRouterImpl(fragment = this)
    }

    private var phone: String? = null
    private var otp: String? = null
    private var resetPassword = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        phone = requireArguments().getString(PHONE)
        otp = requireArguments().getString(OTP)
        resetPassword = requireArguments().getBoolean(RESET_PASSWORD)
    }

    override fun viewBindingInflate(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentRegistrationFinishBinding {
        return FragmentRegistrationFinishBinding.inflate(
            inflater, container, false
        ).apply {
            this.root.setContent {
                RegistrationFinishScreen(
                    onRegisterClick = { password ->
                        if (resetPassword) {
                            viewModel.resetPassword(
                                otp = requireNotNull(otp),
                                phone = requireNotNull(phone).formatPhone(),
                                password = password
                            )
                        } else {
                            viewModel.registration(
                                otp = requireNotNull(otp),
                                phone = requireNotNull(phone).formatPhone(),
                                password = password
                            )
                        }
                    },
                    onBackClick = { registerFinishRouter.routeBack() }
                )
            }
        }
    }

    override fun viewModelFactory(): ViewModelProvider.Factory {
        return createViewModelFactory {
            AppComponent.factory.registrationFinishFactory.createViewModel()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeData()
        observeLoading()
        observeError()
    }

    private fun observeData() = viewLifecycleOwner.lifecycleScope.launch {
        viewModel.registrationState.collect { result ->
            when(result) {
                RegisterFinishState.Success -> {
                    registerFinishRouter.routeToMenu()
                }
                is RegisterFinishState.ResetPassword -> {
                    showToast(result.message)
                    registerFinishRouter.routeToLogin()
                }
            }
        }
    }

    private fun observeLoading() = viewLifecycleOwner.lifecycleScope.launch {
        viewModel.loadingState.collect { result ->
            when(result) {
                LoadingState.Show -> showLoading()
                LoadingState.Hide -> hideLoading()
            }
        }
    }

    private fun observeError() = viewLifecycleOwner.lifecycleScope.launch {
        viewModel.errorState.collect { result ->
            showToast(result)
        }
    }

    companion object {
        const val OTP = "opt"
        const val PHONE = "phone"
        const val RESET_PASSWORD = "reset_password"
    }
}