package com.app.bccmobile.android.widget.compose

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.absolutePadding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.app.bccmobile.android.R

@Composable
fun NoAccRegisterWidget (
    onRegisterClick: () -> Unit
) {
    Row(
        modifier = Modifier.absolutePadding(top = 24.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(
            text = stringResource(id = R.string.no_account),
            modifier = Modifier.absolutePadding(
                left = 16.dp
            ),
            style = MaterialTheme.typography.caption
        )
        Text(
            text = stringResource(id = R.string.register),
            modifier = Modifier.absolutePadding(
                left = 16.dp
            ).noRippleClickable(onClick = onRegisterClick),
            color = colorResource(id = R.color.green27AE60)
        )
    }
}