package com.app.bccmobile.android.widget.compose

import android.view.LayoutInflater
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import com.app.bccmobile.android.R
import com.app.bccmobile.android.widget.PinEditText

@Composable
fun PinCodeWidget(
    pinCodeLength: Int = 4,
    pinInputState: MutableState<String>,
    onPinEntered: (String) -> Unit
) {
    AndroidView(
        modifier = Modifier.fillMaxWidth()
            .height(height = 2.dp),
        factory = { context ->
            val pinEditText: PinEditText = LayoutInflater.from(context)
                .inflate(R.layout.view_pincode, null, false) as PinEditText
            pinEditText.setOnPinEnteredListener { pinCode ->
                if (pinCode.length <= pinCodeLength) {
                    onPinEntered.invoke(pinCode.toString())
                }
            }
            pinEditText.setMaxLength(pinCodeLength)
            pinEditText.requestFocus()
            pinEditText
        },
        update = { pinEditText ->
            pinEditText.setText(pinInputState.value)
        }
    )
}