package com.app.bccmobile.android.widget.compose

import android.util.Log
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Checkbox
import androidx.compose.material.CheckboxDefaults
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.unit.dp
import com.app.bccmobile.android.R

@Composable
fun CheckBoxItem(
    modifier: Modifier,
    isChecked: Boolean = true,
    formState: MutableState<Boolean>? = null,
    text: String,
    isClickable: Boolean = false
) {
    Log.d("form_state", isChecked.toString())
    Row(
        modifier = modifier,
        verticalAlignment = Alignment.CenterVertically,
    ) {
        Checkbox(
            checked = isChecked,
            onCheckedChange = {
                if (isClickable) {
                    formState?.value = it
                }
            },
            colors = CheckboxDefaults.colors(
                checkedColor = colorResource(id = R.color.green27AE60)
            ),
            enabled = true
        )
        Text(
            text = text,
            modifier = Modifier.padding(4.dp),
            style = MaterialTheme.typography.subtitle1,
            color = colorResource(id = R.color.gray3A3A3F)
        )
    }
}