package com.app.bccmobile.android.feature.auth.pincode

enum class PinCodeScreenState {
    CREATE,
    ENTER,
    CHANGE
}