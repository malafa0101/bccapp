package com.app.bccmobile.android.widget.ui

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)
val Gray3A3A3F = Color(0xFF3A3A3F)
val GrayFAFAFA = Color(0xFFFAFAFA)
val GrayA9ACB0= Color(0xFFA9ACB0)
val GrayD6D9DB= Color(0xFFD6D9DB)
val GrayF0F0F0= Color(0xFF3A3A3F)
val Green27AE60= Color(0xFF27AE60)
val RedB10909= Color(0xFFB10909)