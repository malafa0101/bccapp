plugins {
    id("com.android.application")
    kotlin("android")
    id("kotlin-kapt")
    id("androidx.navigation.safeargs.kotlin")
    id("kotlin-android")
}

repositories {
    jcenter()
    maven { url = uri("https://jitpack.io") }
}

dependencies {
    implementation(project(":shared"))
    implementation("androidx.core:core-ktx:1.5.0")
    implementation("com.google.android.material:material:1.3.0")
    implementation("androidx.appcompat:appcompat:1.3.0")
    implementation("androidx.constraintlayout:constraintlayout:2.0.4")

    implementation("androidx.fragment:fragment:1.3.4")
    implementation("androidx.lifecycle:lifecycle-process:2.3.1")
    implementation("androidx.lifecycle:lifecycle-service:2.3.1")
    implementation("androidx.lifecycle:lifecycle-viewmodel-savedstate:2.3.1")
    implementation("androidx.lifecycle:lifecycle-extensions:2.2.0")

    // Navigation
    implementation("androidx.navigation:navigation-fragment-ktx:2.3.5")
    implementation("androidx.navigation:navigation-ui-ktx:2.3.5")
    //Mask
    implementation("com.redmadrobot:input-mask-android:6.0.0")
    implementation ("org.jetbrains.kotlin:kotlin-stdlib:1.5.10")

    implementation("dev.icerock.moko:mvvm:0.10.1")
    implementation("dev.icerock.moko:mvvm-viewbinding:0.10.1")

    implementation("androidx.legacy:legacy-support-v4:1.0.0")

    implementation("com.russhwolf:multiplatform-settings:0.7.7")

    implementation("com.auth0.android:jwtdecode:2.0.0")

    testImplementation("junit:junit:4.13.2")
    androidTestImplementation("junit:junit:4.13.2")
    testImplementation("androidx.test.ext:junit:1.1.2")
    androidTestImplementation("androidx.test.ext:junit:1.1.2")
    androidTestImplementation("androidx.test:runner:1.3.0")
    androidTestImplementation("androidx.test:rules:1.3.0")


    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.4.2-native-mt")

    val compose_version = "1.0.0-beta08"
    implementation ("androidx.compose.ui:ui:${compose_version}")
    implementation ("androidx.compose.foundation:foundation:${compose_version}")
    implementation ("androidx.compose.runtime:runtime-livedata:${compose_version}")
    implementation ("androidx.compose.material:material:${compose_version}")
    implementation ("androidx.compose.material:material-icons-core:${compose_version}")
    implementation ("androidx.compose.material:material-icons-extended:${compose_version}")
    implementation ("androidx.compose.ui:ui-tooling:$compose_version")

    implementation ("androidx.constraintlayout:constraintlayout-compose:1.0.0-alpha07")
    implementation ("androidx.activity:activity-compose:1.3.0-beta01")

    implementation ("androidx.biometric:biometric:1.1.0")
}

android {
    compileSdkVersion(30)
    defaultConfig {
        applicationId = "com.app.bccmobile.android"
        minSdkVersion(21)
        targetSdkVersion(30)
        versionCode = 1
        versionName = "1.0"

        buildConfigField("String", "BASE_URL", "\"https://dev-sso-api.coloor.net\"")

    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
        }
    }

    packagingOptions {
        exclude("META-INF/*.kotlin_module")
    }

    composeOptions {
        kotlinCompilerExtensionVersion = "1.0.0-beta08"
        kotlinCompilerVersion = "1.5.10"
    }

    kotlinOptions {
        jvmTarget = "1.8"
        useIR = true
    }

    buildFeatures {
        compose = true
        dataBinding = true
        viewBinding = true
    }
}